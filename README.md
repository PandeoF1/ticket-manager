# [Ticket - Manager](https://ticket-demo.pandeo.fr)
> Cursed work during internship

## File :<br />
> - [config](https://github.com/PandeoF1/ticket-manager/tree/main/config) -> Configuration file <br />
> - [css](https://github.com/PandeoF1/ticket-manager/tree/main/css) -> CSS file <br />
> - [js](https://github.com/PandeoF1/ticket-manager/tree/main/js) -> JS file (Jquery & canvajs) <br />
> - [sql](https://github.com/PandeoF1/ticket-manager/tree/main/sql) -> SQL file (Database template) <br />
> - [src](https://github.com/PandeoF1/ticket-manager/tree/main/src) -> PHP Addon (PHPMailer) <br />
> - [index.php](https://github.com/PandeoF1/ticket-manager/blob/main/index.php) -> website (All in one) <br />
> - [fetch.php](https://github.com/PandeoF1/ticket-manager/blob/main/fetch.php) -> ticket-list (Dynamic research bar..) <br />
> - [.htaccess](https://github.com/PandeoF1/ticket-manager/blob/main/.htaccess) -> redirect url <br />


## Apache Configuration File :
```` 
<VirtualHost *:80>
        <Directory /var/www/ticket-manager/>
              Options Indexes FollowSymLinks MultiViews
               AllowOverride All
               Require all granted
        </Directory>

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/ticket-manager/
        ServerName domain
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
````
## Requirements :
 > - apache <br />
 > - mysql/mariadb <br />
 > - php <br />
 > - SMTP Server (Optional -> (Not for the moment x) ))<br />

### Demo :
- User : admin
- Pass : admin

### Todo :
- [ ] Clean the code.
- [ ] Move mail configuration in config file..
- [ ] Installation guide.

### Other :

That's my first real website. I have try to work with redirection and other tools like that. I know.. the code is not pretty cool. But that's work x).
I have made that during an internship (2021/xx/xx) at [CHIMB](http://www.chimb.fr/) (French Hospital Center).