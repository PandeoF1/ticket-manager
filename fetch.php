<?php

require "config/mysql.conf.php";
require "config/ticket.conf.php";

$connect = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
$output = '';
$result_post = array_values($_POST);



if (isset($_POST["list_rows"])) $list_rows = $_POST["list_rows"];
else $list_rows = 25;


$result = mysqli_query($connect, "SELECT * FROM statut");
while ($row = mysqli_fetch_array($result)) {
	$statut_type[$row[1]] = $row[0];
}
$result = mysqli_query($connect, "SELECT * FROM statut");
while ($row = mysqli_fetch_array($result)) {
	$statut_name[$row[0]] = $row[1];
}
$result = mysqli_query($connect, "SELECT * FROM service_demandeur");
while ($row = mysqli_fetch_array($result)) {
	$service_color[$row[1]] = $row[3];
}
$count = 3;
$count_set = 0;
$query = "SELECT * FROM intervention WHERE etablissement = '" . $_POST['etablissement'] . "' AND service = '" . $_POST['service'] . "' AND (";
while (isset($result_post[$count])) {
	if (strlen($result_post[$count]) != 0) {
		if ($count_set == 0) {
			$query .= "" . $result_post[$count + 1] . " LIKE '%" . $result_post[$count] . "%' ";
			$count_set = 1;
		} else {
			$query .= "AND " . $result_post[$count + 1] . " LIKE '%" . $result_post[$count] . "%' ";
		}
	}

	$count++;
	$count++;
}
$query .= ") ORDER BY id DESC limit $list_rows";

if ($count_set == 0) {
	$query = "SELECT * FROM intervention WHERE etablissement = '" . $_POST['etablissement'] . "' AND service = '" . $_POST['service'] . "' ORDER BY id DESC limit $list_rows";
}
$result = mysqli_query($connect, $query);
if (mysqli_num_rows($result) > 0) {
	$output .= '
					<table class="table tableresponsive" style="display:block;overflow:auto;-">
					<thead >
						<tr>
							<th>Id</th>
							<th>Date</th>
							<th>Service</th>
							<th>Nom</th>
							<th>Nature Demande</th>
							<th>Statut</th>
							<th>Technicien</th>
							<th>Detail</th>
						</tr>
					</thead>
				';
	while ($row = mysqli_fetch_array($result)) {
		if($service_color[$row["service_demandeur"]] == "default") $output .= '<tr>';
		else $output .= '<tr style="background-color:'.$service_color[$row["service_demandeur"]].';">';
		$output .='
				<td>' . $row["id"] . '</td>
				<td>' . $row["date_demande"] . '</td>
				<td>' . $row["service"] . '</td>
				<td>' . $row["nom_demandeur"] . '</td>
				<td>' . $row["nature_demande"] . '</td>
				<td>' . $statut_name[$row["statut"]] . '</td>
				<td>' . $row["nom_technicien"] . '</td>	
				<td><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="window.open(`./'.$row["id"].'`);">🔎</button></td></td>
			</tr>
		';
	}
	$output .= "</table>";
	echo $output;
} else {
	echo 'Aucun ticket a était trouver !';
}
