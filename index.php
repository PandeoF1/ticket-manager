  <!-- Gestionnaire des tickets | Par : R.Anthony & N.Théo -->

  <?php
session_start();
require "config/mysql.conf.php";
require "config/ticket.conf.php";

require 'src/PHPMailer.php';
require 'src/SMTP.php';
require 'src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;


#--------- Gestion des div ----------
$div_status = 0;
$div = array_fill(0, 100, "0");
#--------- Gestion des div ----------
$db_conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
$url_parse = explode("/", trim($_SERVER["REQUEST_URI"], "/"));
$query_result = mysqli_query($db_conn, "SELECT * FROM `etablissement`");

#Logout button
if (isset($_POST["logout"])) {
    $_SESSION["authenticated"] = 0;
    $_SESSION["username"] = null;
}
if($url_parse[0] == "stats"){
  $div["acceuil"] = 0;
  $div["service"] = 0;
  $div["intervention"] = 0;
  $div["intervention_view"] = 0;
  $div["stats"] = 1;
  $div_status = 1;
}
if (isset($_POST["login_button"])) {
    header("location: /login/");
}
$count_ = 0;
while ($query_row = mysqli_fetch_row($query_result)) {
    $etablissement_list[$count_] = $query_row[1];
    $count_++;
    if ($url_parse[0] == $query_row[1]) {
        $etablissement_logo = $query_row[2];
        $etablissement_used = $query_row[1];
        $div["acceuil"] = 0;
        $div["service"] = 1;
        $div_status = 1;
    }
}
if (isset($etablissement_used)) {
    $query_result = mysqli_query(
        $db_conn,
        "SELECT * FROM `service` WHERE `etablissement` LIKE '%| $etablissement_used |%'"
    );
    $count_ = 0;
    while ($query_row = mysqli_fetch_row($query_result)) {
        $service_list[$count_] = $query_row[1];
        $service_list_description[$count_] = $query_row[4];
        $count_++;
        if ($url_parse[1] == $query_row[1]) {
            $service_used = $query_row[1];
            $div["acceuil"] = 0;
            $div["service"] = 0;
            $div["intervention"] = 1;
            $div_status = 1;
            if ($url_parse[2] == "view") {
                //if(isset($url_parse[3])){
                $div["acceuil"] = 0;
                $div["service"] = 0;
                $div["intervention"] = 0;
                $div["intervention_view"] = 1;
                //}
                if (isset($url_parse[3])) {
                    $div["acceuil"] = 0;
                    $div["service"] = 0;
                    $div["intervention"] = 0;
                    $div["intervention_view"] = 0;
                    $div["intervention_ticket_view"] = 1;
                }
            }
        }
    }
} elseif ($url_parse[0] == "admin") {
    if ($_SESSION["authenticated"] == 1) {
        if ($_SESSION["admin"] == 1) {
            $div["acceuil"] = 0;
            $div["service"] = 0;
            $div["intervention"] = 0;
            $div["intervention_view"] = 0;
            $div["admin_service"] = 1;
            $div["admin_login"] = 0;
            $div_status = 1;
        } else {
            header("location: /");
        }
    } else {
        header("location: /login/");
    }
} elseif ($url_parse[0] == "login") {
    $div["acceuil"] = 0;
    $div["service"] = 0;
    $div["intervention"] = 0;
    $div["intervention_view"] = 0;
    $div["admin_service"] = 0;
    $div["admin_login"] = 1;
    $div_status = 1;
}

if ($div_status == 0) {
    $div["acceuil"] = 1;
}
?>

<!DOCTYPE HTML>
<HTML>

<script src="/js/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" name="viewport" content="width=device-width, initial-scale=1">
<link href="/css/ticket.css" rel="stylesheet" type="text/css">

<head>
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
  <title>
    <?php echo $title; ?>
  </title>
</head>



<body>
  <!-- Affichage acceuil -->
  <?php if ($_SESSION["authenticated"] == 1) { ?>
    <div class="admin_user">
      <form action="" method="post">
        <div style="border-bottom:2px solid silver">
          <h3>Utilisateur<button class="quit_button" type="submit" id="logout-image" name="logout"><img class="img_button" src="/img/logout.svg"/></button></h3>
        </div>
        <h5><?php echo $_SESSION["username"]; ?></h5>
      </form>
    </div>
  <?php } elseif ($div["admin_login"] == 0) { ?>
    <div class="admin_user">
      <form action="" method="post">
        <h5><button name="login_button" class="button_login" value="<?php echo $count_; ?>" type="submit">Se connecter</button></h5>
      </form>
    </div>
  <?php } ?>
  <?php if ($div["acceuil"] == 1) { ?>
    <div class="accueil">

      <!--<head>
        <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="200" height="150"></a>
      </head>-->
      <!-- Affichage des établissements -->
    </div>
    <div class="accueil_title">
      <h1>Bon de travaux :</h1>
    </div>
    <?php
    $count = 0; //Variable pour posistion button
    $count_ = 0; //Variable pour value du button
    $px = 20;
    foreach ($etablissement_list as $etablissement_name) {

        $count++;
        $count_++;
        ?>
      <div class="etablissement etablissement<?php echo $count; ?>" style="top:<?php echo $px; ?>%">
        <h1>
          <?php echo $etablissement_name; ?>
        </h1>
        <div class="button_box_etablissement">
          <a href="/<?php echo $etablissement_name; ?>/">
            <br>
            <button class="button_etablissement" name="buttton_etablissement" value="<?php echo $count_; ?>" type="submit">
              <h3>Selectionner</h3>
            </button>
          </a>
        </div>
        <br>
      </div>
    <?php if ($count == 3) {
        $count = 0;
        $px += 32.5;
    }
    }
    ?>

    </div>
  <?php } ?>
  <?php if ($div["service"] == 1) { ?>
    <div class="service">

      <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="10%" height="10%"></a>

    </div>
    <div class="service_title">
      <h1>Bon de travaux :</h1>
    </div>
    <!-- Affichage des services -->
    <?php
    $count = 0; //Variable pour posistion button
    $count_ = 0; //Variable pour value du button
    $px = 20;
    foreach ($service_list as $service_name) {
        $count++; ?>
      <div class="etablissement etablissement<?php echo $count; ?>" style="top:<?php echo $px; ?>%">
        <h1>
          <?php echo $service_name; ?>
          <textarea class="desc_box" value="" readonly><?php if (
              $service_list_description[$count_] != null
          ) {
              echo "(" . $service_list_description[$count_] . ")";
          } ?></textarea>
        </h1>
        <div class="button_box_etablissement">
          <a href="/<?php echo $etablissement_used; ?>/<?php echo $service_name; ?>">
            <br>
            <button name="button_service" class="button_service" type="submit">Selectionner</button>
          </a>
        </div>
        <br>
      </div>
    <?php
    if ($count == 3) {
        $count = 0;
        $px += 32.5;
    }
    $count_++;

    }
    ?>

    </div>
  <?php } ?>
  <?php if ($div["intervention"] == 1) { ?>
    <div class="intervention">
      <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="10%" height="10%"></a>

      <div class="box_intervention">
        <form action="" method="post">
          <!-- Récupération de la bdd -->
          <?php
          $count = 0;
          $query_result = mysqli_query(
              $db_conn,
              "SELECT * FROM `service_demandeur` WHERE `etablissement` LIKE '%| $etablissement_used |%'"
          );
          while ($query_row = mysqli_fetch_row($query_result)) {
              $service_demandeur_list[$count - 1] = $query_row[1];
              $count++;
          }
          $count = 0;
          $query_result = mysqli_query(
              $db_conn,
              "SELECT * FROM `nature_demande` WHERE `etablissement` LIKE '%| $etablissement_used |%' AND `permission` LIKE '%| $service_used |%'"
          );
          while ($query_row = mysqli_fetch_row($query_result)) {
              $nature_demande_list[$count - 1] = $query_row[1];
              $count++;
          }
          ?>
          <h1>Bon de demande d'intervention</h1>
          <div class="demand">
            <br>
            <h3>Nom demandeur :
              <div class="autocomplete" >
                <form action="" method="post">
                  <input type="text" name="intervention_user" placeholder="Saisir votre nom puis votre prénom" id="name_demand" class="name_demand" autocomplete="off" required>
                </form>
              </div>
            </h3>
            <br>
            <h3>Service demandeur :
              <select name="intervention_service_demandeur" class="service_list" id="service_list" required>
                <option selected disabled>Votre service</option>
                <!--Creation de la liste déroulante des services -->
                <?php foreach (
                    $service_demandeur_list
                    as $service_demandeur_name
                ) {
                    echo "<option value='$service_demandeur_name'>$service_demandeur_name</option>";
                } ?>
              </select>
              <!------------------------------------------------->
            </h3>
            <!--<br>
            <h3>Nature de la demande :
              <select name="intervention_nature_demande" class="nature_demande" id="nature_demande" required>
                <option selected disabled>Choisir la nature</option>
                <?php
      //foreach ($nature_demande_list as $nature_demande_name) {
      //  echo "<option value='$nature_demande_name'>$nature_demande_name</option>";
      //}
      ?>
              </select>
            </h3>-->
            <br>
            <input type="checkbox" id="personnel" name="intervention_personnel" unchecked> Voir avec le
            personnel avant intervention. </input>
            <br><br>
            <hr>
            <br>
            <h3>Detail de la demande :
              <br>
              <textarea name="intervention_reparation" class="detail" cols="75" rows="8" required></textarea>
            </h3>
            <br>
            <button type="submit" class="send" name="intervention_sendbutton" value="">
              <h5>Valider</h5>
            </button>
          </div>
          <?php 
          if (isset($_POST["intervention_sendbutton"])) {

              //sql
              if ($_POST["intervention_personnel"] == "on") {
                  $request_voir_peronnel = 1;
              } else {
                  $request_voir_peronnel = 0;
              }
              $dates = new DateTime();
              
              $request_date = $dates->format("Y-m-d H:i");
              $request_etablissement = $etablissement_used;
              $request_nature_demande = $_POST["intervention_nature_demande"]; //Plus besoin 
              $request_nom_demandeur_ = $_POST["intervention_user"];
              $request_reparation_ = $_POST["intervention_reparation"];
              $request_nom_demandeur = mysqli_real_escape_string($db_conn, $_POST["intervention_user"]);
              $request_reparation = mysqli_real_escape_string($db_conn, $_POST["intervention_reparation"]);
              $request_service_demandeur = $_POST["intervention_service_demandeur"];
              $request_service = $service_used;
              
              mysqli_multi_query(
                  $db_conn,
                  "INSERT INTO `intervention` (`etablissement`,`service`, `date_demande`, `service_demandeur`, `nom_demandeur`,`nature_demande`, `reparation`, `statut`, `voir_personnel`) VALUES ('$request_etablissement','$request_service', '$request_date', '$request_service_demandeur', '$request_nom_demandeur','$request_nature_demande', '$request_reparation', '0','$request_voir_peronnel')"
              );
              

              $result = mysqli_query(
                  $db_conn,
                  "SELECT * FROM `intervention` WHERE (`etablissement`, `date_demande`, `service_demandeur`, `nom_demandeur`,`nature_demande`, `reparation`, `statut`, `voir_personnel`) = ('$request_etablissement', '$request_date', '$request_service_demandeur', '$request_nom_demandeur','$request_nature_demande', '$request_reparation', '0','$request_voir_peronnel') LIMIT 1"
              );
              $row = mysqli_fetch_row($result);
              $id = $row[0];
              $result = mysqli_query(
                  $db_conn,
                  "SELECT * FROM `utilisateur` WHERE `utilisateur`.`nom` = '$request_nom_demandeur' LIMIT 1"
              );
              if ($result->num_rows >= 1) {
                  mysqli_query(
                      $db_conn,
                      "UPDATE `utilisateur` SET `nom` = '$request_nom_demandeur', `service` = '$request_service_demandeur', `etablissement` = '$request_etablissement' WHERE `utilisateur`.`nom` = '$request_nom_demandeur'"
                  );
                  //update
              } else {
                  mysqli_query(
                      $db_conn,
                      "INSERT INTO `utilisateur` (`nom`, `service`, `etablissement`) VALUES ( '$request_nom_demandeur', '$request_service_demandeur', '$request_etablissement') "
                  );
                  //insert
              }
              //send mail

              $_query_result = mysqli_query( //Récupération mail du service
                $db_conn,
                "SELECT * FROM `service` WHERE `etablissement` LIKE '%| $etablissement_used |%' AND `nom` LIKE '$service_used'"
              );
              while ($_query_row = mysqli_fetch_row($_query_result)) {
                  $mail_service = $_query_row[3];
              }
              try {
                $mail = new PHPMailer(true);
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
                $mail->addAddress($mail_service);
                //$mail->SMTPDebug = SMTP::DEBUG_SERVER; //log
                $mail->isSMTP();
                $mail->Host       = 'smtp1.sil.fr';
                $mail->SMTPAuth   = false;
                $mail->Username   = 'user@example.com';
                $mail->Password   = 'secret';
                $mail->Port       = 25;
                
                $mail->CharSet = "UTF-8";
                $actual_link = "http://$_SERVER[HTTP_HOST]/$etablissement_used/$service_used/view/$id/";
                $mail->setFrom($mail_service, "Bon d'intervention");
                $mail->isHTML(true);
                $mail->Subject = "Nouveau ticket - $request_date";
                $mail->Body    = "<p style='text-align: center;'><span style='text-decoration: underline;'><strong>ID :</strong></span> <em><a href='$actual_link'>$id</a></em></p>
                <p style='text-align: center;'><span style='text-decoration: underline;'><strong>Demandeur :</strong></span> <em>$request_nom_demandeur_</em></p>
                <p style='text-align: center;'><strong><span style='text-decoration: underline;'>Service :</span></strong> <em>$request_service_demandeur</em></p>
                <p style='text-align: center;'><strong><span style='text-decoration: underline;'>Description :</span></strong> <em><br>".wordwrap($request_reparation_, 125, "<br>", true)."</em></p>
                <p style='text-align: center;'>&nbsp;</p>
                <p style='text-align: center;'>&nbsp;</p>
                <p style='text-align: center;'>&nbsp;</p>
                <p style='text-align: center;'>-------------------------------------------------------------------------</p>
                <p style='text-align: center;'>Gestionnaire de ticket</p>
                <p style='text-align: center;'>-------------------------------------------------------------------------</p>";
                $mail->Send();
              } catch (Exception $e) {
                  echo 'Exception reçue : ',  $e->getMessage(), "\n";
              }
              
              mysqli_commit($db_conn);
            ?>
            <div class="alert_intervention">
              <span class="succesfull" onclick="this.parentElement.style.display='none';">&times;</span>
              La demande a bien été prise en compte (<a href="<?php echo "
                        /$etablissement_used/$service_used/view/$id";
              //id ici
              ?>">Détails du ticket, cliquez ici</a>) |
              Numéro du ticket :
              <?php echo $id; ?>.
            </div>
          <?php
          } ?>
          <br>
        </form>
      </div>
      <div class="view_bon">
        <h1>Aperçu des bons :</h1>
        <br>
        <!--<button class="view viewpersonnal" onclick="mes_bon();" value="">
          <h3>Voir mes bons</h3>
        </button>
        <br><br>
        <button class="view viewservice" onclick="mes_services();" value="">
          <h3>Voir les bons de mon service</h3>
        </button>
        <br><br>-->
        <a href=<?php echo "/$etablissement_used/$service_used/view/"; ?>><button class="view viewall" value="">
            <h3>Voir
              tous les bons</h3>
          </button></a>
        <br><br>
      </div>
    </div>
  <?php } ?>
  <!-- Affichage des tickets -->

  <?php if ($div["intervention_view"] == 1) { ?>
    <div id="intervention_view" class="intervention_view">
      <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="10%" height="10%"></a>
      <div class="intervention_view_box">
        <h1>Visualisations des bons :</h1>
        <br>
        <div class="result" id="result" style="width:100%;"></div>
        <div style="clear:both"></div>
        <br>
      </div>
      <div class="filter">
        <h1>Filtre :</h1>
        <br>
        <?php if ($_SESSION["authenticated"] == 1) { ?>
          <div class="filtre_tech_col_1">
          <input type="text" class="filtre__tech_id" id="filtre_id" placeholder="Id du ticket">
          <input type="text" class="filtre_tech_nom" id="filtre_nom" placeholder="Votre nom">
          <input type="text" class="filtre_tech_service" id="filtre_service" placeholder="Service">
          <input type="text" class="filtre__tech_date" id="filtre_date" placeholder="Date"></div>
          <div class="filtre_tech_col_2">
          <input type="text" class="filtre_tech_type" id="filtre_type" placeholder="Nature Demande">
          <select name="filtre_statut" class="filtre_tech_statut" id="filtre_statut">
          <option value='-1'>Tous</option>
            <?php
            $_query_result = mysqli_query($db_conn, "SELECT * FROM `statut`");
            while ($_query_row = mysqli_fetch_row($_query_result)) {
                echo "<option value='$_query_row[0]'>$_query_row[1]</option>";
            }
            ?>
          </select>
          
          <input type="text" class="filtre_tech_tech" id="filtre_tech" placeholder="Technicien">
          <input type="text" class="filtre_tech_details" id="filtre_details" placeholder="Details">
          <input type="text" class="filtre_tech_remarque" id="filtre_remarque" placeholder="Remarque"></div>
          <div class="filtre_recherche_box">
          <button class="filtre_tech_recherche" id="reload_data_research" value="">Rechercher</button>
          <select name="list_rows" class="list_rows" id="list_rows">
            <?php
            echo "<option selected value='25'>25</option>";
            for ($i = 50; $i <= 500; $i += 25) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
          </select>
          </div>
          <br>
        <?php } else { ?>
          <div class="filtre_client">
          <input type="text" class="filtre_id" id="filtre_id" placeholder="Id du ticket"><br><br>
          <input type="text" class="filtre_nom" id="filtre_nom" placeholder="Votre nom"><br><br>
          <input type="text" class="filtre_service" id="filtre_service" placeholder="Service"><br><br>
          <input type="text" class="filtre_date" id="filtre_date" placeholder="Date"><br><br>
        </div>
        <div class="filtre_recherche_box">
          <button class="filtre_recherche" id="reload_data_research" value="">Rechercher</button>
          <select name="list_rows" class="list_rows" id="list_rows">
            <?php
            echo "<option selected value='25'>25</option>";
            for ($i = 50; $i <= 500; $i += 25) {
                echo "<option value='$i'>$i</option>";
            }
            ?>
          </select>
          </div>
        <?php } ?>
        <br>
      </div>
    </div>
  <?php } ?>
  <?php if ($div["admin_service"] == 1) { ?>
    <div class="admin_service" id="GDFG">

      <!--<head>
      <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="10%" height="10%"></a>
      </head>
       Affichage des établissements -->
      <div class="admin_box">
        <div class="admin_eta">
          <h1>Etablissement :
            <button class="add_button" onclick="config_open(`etablissement_create`);" type="submit">➕</button>
          </h1>
          <table class="table tableeta">
            <thead>
              <tr>
                <th>Nom</th>
                <th>Service</th>
                <th>Modifier</th>
              </tr>
            </thead>
            <tbody id="body_1">
              <?php
              if (isset($_POST["etablissement_change_config"])) {

                  //Modification
                  $_post_result = mysqli_query(
                      $db_conn,
                      "SELECT * FROM `service`"
                  );
                  while ($_post_row = mysqli_fetch_row($_post_result)) {
                      $_post_row[2] = str_replace(
                          "| " .
                              $_POST["etablissement_change_config_nom"] .
                              " |",
                          "",
                          $_post_row[2]
                      );
                      mysqli_multi_query(
                          $db_conn,
                          "UPDATE `service` SET `etablissement` = '$_post_row[2]' WHERE id = '$_post_row[0]'"
                      );
                      mysqli_commit($db_conn);
                  }
                  foreach (
                      $_POST["etablissement_change_config_list_service"]
                      as $etablissement_list_to_use
                  ) {
                      $_post_result = mysqli_query(
                          $db_conn,
                          "SELECT * FROM `service` WHERE id = '$etablissement_list_to_use'"
                      );
                      $_list = "";
                      while ($_post_row = mysqli_fetch_row($_post_result)) {
                          $_list .=
                              $_post_row[2] .
                              "| " .
                              $_POST["etablissement_change_config_nom"] .
                              " |";
                          mysqli_multi_query(
                              $db_conn,
                              "UPDATE `service` SET `etablissement` = '$_list' WHERE id = '$_post_row[0]'"
                          );
                          mysqli_commit($db_conn);
                      }
                  }

                  mysqli_commit($db_conn);
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" /> <?php
              }
              if (isset($_POST["etablissement_change_config_delete"])) {

                  //Supression
                  mysqli_query(
                      $db_conn,
                      "DELETE FROM `etablissement` WHERE `etablissement`.`id` = '" .
                          $_POST["etablissement_change_config_delete"] .
                          "'"
                  );
                  mysqli_commit($db_conn);
                  mysqli_query(
                      $db_conn,
                      "UPDATE service SET etablissement = REPLACE(etablissement, '| " .
                          $_POST["etablissement_change_config_nom"] .
                          " |', '')"
                  );
                  mysqli_commit($db_conn);
                  mysqli_query(
                      $db_conn,
                      "UPDATE technicien SET etablissement = REPLACE(etablissement, '| " .
                          $_POST["etablissement_change_config_nom"] .
                          " |', '')"
                  );
                  mysqli_commit($db_conn);

                  //Remove établissement from services where etablissement = %etablissement a supr%
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" /> <?php
              }
              if (isset($_POST["etablissement_create"])) {
                  mysqli_query(
                      $db_conn,
                      "INSERT INTO `etablissement` (`id`, `nom`, `logo`) VALUES (NULL, '" .
                          $_POST["etablissement_create_nom"] .
                          "', '')"
                  );
                  mysqli_commit($db_conn);

                  //Ajout de l'établissement au créateur de celui-ci
                  $_post_result = mysqli_query(
                      $db_conn,
                      "SELECT * FROM `technicien` WHERE utilisateur = '" .
                          $_SESSION["username"] .
                          "'"
                  );
                  $_list = "";
                  while ($_post_row = mysqli_fetch_row($_post_result)) {
                      $_list .=
                          $_post_row[4] .
                          "| " .
                          $_POST["etablissement_create_nom"] .
                          " |";
                      mysqli_multi_query(
                          $db_conn,
                          "UPDATE `technicien` SET `etablissement` = '$_list' WHERE utilisateur = '" .
                              $_SESSION["username"] .
                              "'"
                      );
                      mysqli_commit($db_conn);
                  }

                  //mysqli_query($db_conn, "UPDATE `technicien` SET `etablissement` = CONCAT(ISNULL(`etablissement`, ''),'| " . $_POST['etablissement_create_nom'] . " |') WHERE utilisateur = '" . $_SESSION['username'] . "'");
                  //mysqli_commit($db_conn);
                  //Revoir tous les updataUPDATE `etablissement` SET `nom` = CONCAT(`nom`,"B") WHERE `etablissement`.`id` = 10;
                  //UPDATE table SET nom_colonne = REPLACE(nom_colonne, 'ancien texte', 'texte de remplacement'
                  //mysqli_query($db_conn, "UPDATE `technicien` SET `etablissement` = CONCAT(`etablissement`,'| " . $_POST['etablissement_create_nom'] . " |') WHERE utilisateur = '" . $_SESSION['username'] . "'");
                  //mysqli_commit($db_conn);
                  //Ajout de l'établissement au service selectionner
                  foreach (
                      $_POST["etablissement_create_list_service"]
                      as $etablissement_list_to_use
                  ) {
                      $_post_result = mysqli_query(
                          $db_conn,
                          "SELECT * FROM `service` WHERE id = '$etablissement_list_to_use'"
                      );
                      $_list = "";
                      while ($_post_row = mysqli_fetch_row($_post_result)) {
                          $_list .=
                              $_post_row[2] .
                              "| " .
                              $_POST["etablissement_create_nom"] .
                              " |";
                          mysqli_multi_query(
                              $db_conn,
                              "UPDATE `service` SET `etablissement` = '$_list' WHERE id = '$_post_row[0]'"
                          );
                          mysqli_commit($db_conn);
                      }
                  }
              }

              $result = mysqli_query(
                  $db_conn,
                  "SELECT * FROM `technicien` WHERE utilisateur = '" .
                      $_SESSION["username"] .
                      "'"
              );

              while ($row = mysqli_fetch_row($result)) {
                  $user_permission_etablissement .= $row[4];
              }

              $result = mysqli_query($db_conn, "SELECT * FROM `etablissement`");

              while ($row = mysqli_fetch_row($result)) {
                  $button_config_status = strpos(
                      $user_permission_etablissement,
                      "| " . $row[1] . " |"
                  );
                  $results = mysqli_query(
                      $db_conn,
                      "SELECT * FROM `service` WHERE etablissement LIKE '%| " .
                          $row[1] .
                          " |%'"
                  );
                  $admin_service_nom = "-";
                  while ($rows = mysqli_fetch_row($results)) {
                      if ($admin_service_nom != "-") {
                          $admin_service_nom .= ", " . $rows[1];
                      } else {
                          $admin_service_nom = $rows[1];
                      }
                  }
                  echo "<tr>";
                  echo "<td><h4>" . $row[1] . "</h4></th>";
                  echo "<th><p>" . $admin_service_nom . "</p></th>";
                  if ($button_config_status !== false) {
                      echo '<th><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="config_open(`etablissement_open_' .
                          $row[0] .
                          '`);">🔧</button></th>';
                  } else {
                      echo "<th>✖️</th>";
                  }
                  echo "</tr>";
                  if ($button_config_status !== false) { ?>
                  <div id="<?php echo "etablissement_open_" .
                      $row[0]; ?>" class="modal">
                    <div class="modal-content">
                      <div onclick="close_windows('etablissement_open_<?php echo $row[0]; ?>');"> <span class="close">&times;</span></div>
                      <p>

                        <?php
                        echo "";
                        echo "<br><hr class='new1'>";
                        echo "<br>";
                        echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><div class='name_area'><input type='text' class='eta_name' name='etablissement_change_config_nom' readonly value='$row[1]'> </h1>";
                        echo "<br>";
                        echo "<h1>Service : <br>";
                        echo "<select class='eta_select' name='etablissement_change_config_list_service[]' id='etablissement_list_service' multiple>";
                        $result_ = mysqli_query(
                            $db_conn,
                            "SELECT * FROM `service`"
                        );

                        while ($row_ = mysqli_fetch_row($result_)) {
                            $etablissemet_service_used = strpos(
                                $row_[2],
                                "| " . $row[1] . " |"
                            );
                            if ($etablissemet_service_used !== false) {
                                echo "<option selected value='$row_[0]'>$row_[1]  </option>";
                            } else {
                                echo "<option value='$row_[0]'>$row_[1] ($row_[4])</option>";
                            }
                        }
                        echo "</select></h1>";
                        echo "<br><br>";
                        echo "<button name='etablissement_change_config' class='modal_submit' type='submit'>Modifier</button>";
                        echo "</form>";
                        echo "<form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'>";
                        echo "<input type='text' hidden class='eta_name' name='etablissement_change_config_nom' readonly value='$row[1]'>";
                        echo "<button class='modal_delete' value='$row[0]' name='etablissement_change_config_delete' type='submit'>🗑️</button>";
                        echo "</form>";
                        ?></p>
                    </div>

                  </div>
              <?php }
              }
              ?>
              <div id="etablissement_create" class="modal">
                <div class="modal-content">
                  <div onclick="close_windows('etablissement_create');"> <span class="close">&times;</span></div>
                  <p>

                    <?php
                    echo "<br><hr class='new1'>";
                    echo "<br>";
                    echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><div class='name_area'><input type='text' class='eta_name' name='etablissement_create_nom' placeholder='Nom établissement' required></h1>";
                    echo "<br>";
                    echo "<h1>Service :<h1>";
                    echo "<select class='eta_select' name='etablissement_create_list_service[]' id='etablissement_create_list_service' multiple>";
                    $result_ = mysqli_query(
                        $db_conn,
                        "SELECT * FROM `service`"
                    );

                    while ($row_ = mysqli_fetch_row($result_)) {
                        $etablissemet_service_used = strpos(
                            $row_[2],
                            "| " . $row[1] . " |"
                        );
                        echo "<option value='$row_[0]'>$row_[1] ($row_[4])</option>";
                    }
                    echo "</select></h1>";
                    echo "<br><br>";
                    echo "<button name='etablissement_create' class='modal_submit' type='submit'>Ajouter</button>";
                    echo "</form>";
                    echo "<br>";
                    ?></p>
                </div>

              </div>
            </tbody>
          </table>
        </div>
        <div class="admin_serv">
          <h1>Service :
            <button class="add_button" onclick="config_open(`service_create`);" type="submit">➕</button>
          </h1>
          <table class="table tableserv">
            <thead>
              <tr>
                <th>Nom</th>
                <th>Etablissement</th>
                <th>Modifier</th>
              </tr>
            </thead>
            <tbody id="body_1">
              <?php
              if (isset($_POST["service_delete"])) {

                  //Supression
                  mysqli_multi_query(
                      $db_conn,
                      "DELETE FROM `service` WHERE `service`.`id` = '" .
                          $_POST["service_delete"] .
                          "'"
                  );
                  mysqli_multi_query(
                      $db_conn,
                      "UPDATE technicien SET service = REPLACE(service, '" .
                          $_POST["service_delete_nom"] .
                          "', '')"
                  );
                  mysqli_commit($db_conn);
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" /> <?php
              } //Supression sur tous les user :)
              if (isset($_POST["service_create"])) {
                  mysqli_query(
                      $db_conn,
                      "INSERT INTO `service` (`id`, `nom`, `etablissement`, `mail`, `Descriptions`) VALUES (NULL, '" .
                          $_POST["service_create_nom"] .
                          "', '', '" .
                          $_POST["service_create_mail"] .
                          "', '" .
                          $_POST["service_create_description"] .
                          "')"
                  ); ?>
                <meta http-equiv="refresh" content="0;url=/admin/" /> <?php
              }
              if (isset($_POST["service_change_config"])) {

                  //Modification service
                  $_post_result = mysqli_query(
                      $db_conn,
                      "SELECT * FROM `service` WHERE id = '" .
                          $_POST["service_change_config_id"] .
                          "'"
                  );

                  while ($_post_row = mysqli_fetch_row($_post_result)) {
                      $_service_last_name = $_post_row[1];
                      mysqli_query(
                          $db_conn,
                          "UPDATE `service` SET `mail` = '" .
                              $_POST["service_change_config_mail"] .
                              "', `nom` = '" .
                              $_POST["service_change_config_nom"] .
                              "', `descriptions` = '" .
                              $_POST["service_change_config_description"] .
                              "' WHERE id = '" .
                              $_POST["service_change_config_id"] .
                              "'"
                      );
                      mysqli_commit($db_conn);

                      $_post_result = mysqli_query(
                          $db_conn,
                          "SELECT * FROM `technicien` WHERE service = '$_service_last_name'"
                      );

                      while ($_post_row = mysqli_fetch_row($_post_result)) {
                          mysqli_query(
                              $db_conn,
                              "UPDATE `technicien` SET `service` = '" .
                                  $_POST["service_change_config_nom"] .
                                  "' WHERE id = '" .
                                  $_post_row[0] .
                                  "'"
                          );
                          mysqli_commit($db_conn);
                      }
                  }

                  //Modification de tous les user
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" /> <?php
              }

              $result = mysqli_query($db_conn, "SELECT * FROM `service`");

              while ($row = mysqli_fetch_row($result)) {

                  $service_list_exploded = null;
                  $service_list_explode = explode("|", $row[2]);
                  foreach ($service_list_explode as $_) {
                      $service_list_exploded .= $_;
                  }
                  echo "<tr>";
                  echo "<td><h4>" . $row[1] . "</h4></th>";
                  echo "<th><p>" . $service_list_exploded . "</p></th>";
                  echo '<th><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="config_open(`service_open_' .
                      $row[0] .
                      '`);">🔧</button></th>';
                  echo "</tr>";
                  ?>
                <div id="<?php echo "service_open_" .
                    $row[0]; ?>" class="modal">
                  <div class="modal-content">
                    <div onclick="close_windows('service_open_<?php echo $row[0]; ?>');"> <span class="close">&times;</span></div>
                    <p>

                      <?php
                      echo "<br><hr class='new1'>";
                      echo "<br>";
                      echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><div class='name_area'><input type='text' class='eta_name' name='service_change_config_nom' value='$row[1]'><div class='name_area'><input type='text' hidden class='eta_name' name='service_change_config_id' value='$row[0]'></h1>";
                      echo "<br>";
                      echo "<h1>Description : <br><textarea type='text' class='eta_name' name='service_change_config_description'>$row[4]</textarea></h1>";
                      echo "<br>";
                      echo "<h1>Mail : <br><input type='text' class='eta_name' name='service_change_config_mail' value='$row[3]'></h1>";
                      echo "<br><br>";
                      echo "<button name='service_change_config' class='modal_submit' type='submit'>Modifier</button>";
                      echo "</form>";
                      echo "<form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'>";
                      echo "<input type='text' hidden class='eta_name' name='service_delete_nom' value='$row[1]'>";
                      echo "<button name='service_delete' value='$row[0]' class='modal_delete' type='submit'>🗑️</button>";
                      echo "</form>";
                      echo "<br>";
                      ?></p>
                  </div>

                </div>
              <?php
              }
              ?>
              <!-- Création service -->
              <div id="service_create" class="modal">
                <div class="modal-content">
                  <div onclick="close_windows('service_create');"> <span class="close">&times;</span></div>
                  <p>

                    <?php
                    echo "<br><hr class='new1'>";
                    echo "<br>";
                    echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><div class='name_area'><input type='text' class='eta_name' name='service_create_nom' placeholder='Nom du service' required><div class='name_area'></h1>";
                    echo "<br>";
                    echo "<h1>Description : <br><textarea type='text' class='eta_name' name='service_create_description' required></textarea></h1>";
                    echo "<br>";
                    echo "<h1>Mail : <br><input type='text' class='eta_name' name='service_create_mail' value='$row[3]' placeholder='ex : (info@example.fr)'></h1>";
                    echo "<br>";
                    echo "<button name='service_create' class='modal_submit' type='submit'>Ajouter</button>";
                    echo "</form>";
                    echo "<br>";
                    ?></p>
                </div>

              </div>
            </tbody>
          </table>
        </div>
        <div class="admin_tech">
          <h1>Technicien :
            <button class="add_button" onclick="config_open(`tech_create`);" type="submit">➕</button>
          </h1>
          <table class="table tabletech">
            <thead>
              <tr>
                <th>Nom</th>
                <th>Admin</th>
                <th>Mail</th>
                <th>Etablissement</th>
                <th>Service</th>
                <th>Modifier</th>
              </tr>
            </thead>
            <tbody id="body_1">
              <?php
              //Delete Tech
              if (isset($_POST["tech_delete"])) {
                  mysqli_query(
                      $db_conn,
                      "DELETE FROM `technicien` WHERE `technicien`.`id` = '" .
                          $_POST["tech_delete_id"] .
                          "'"
                  );
                  mysqli_commit($db_conn);
              }

              //Modification Tech
              if (isset($_POST["tech_change_config"])) {

                  foreach (
                      $_POST["tech_change_config_list_etablissement"]
                      as $_list_add
                  ) {
                      $_list .= "| " . $_list_add . " |";
                  }
                  if (isset($_POST["tech_change_config_admin"])) {
                      $_admin = "1";
                  } else {
                      $_admin = "0";
                  }
                  if ($_POST["tech_change_config_mdp"] != "") {
                      $hashed = password_hash(
                          $_POST["tech_change_config_mdp"],
                          PASSWORD_DEFAULT
                      );
                      mysqli_query(
                          $db_conn,
                          "UPDATE `technicien` SET `password` = '$hashed' ,`utilisateur` = '" .
                              $_POST["tech_change_config_nom"] .
                              "', `password-to-change` = '1',`service` = '" .
                              $_POST["tech_change_config_service"] .
                              "', `etablissement` = '$_list', `administrateur` = '$_admin', `mail` = '" .
                              $_POST["tech_change_config_mail"] .
                              "' WHERE `technicien`.`id` = '" .
                              $_POST["tech_change_config_id"] .
                              "'"
                      );
                  } else {
                      mysqli_query(
                          $db_conn,
                          "UPDATE `technicien` SET `utilisateur` = '" .
                              $_POST["tech_change_config_nom"] .
                              "', `service` = '" .
                              $_POST["tech_change_config_service"] .
                              "', `etablissement` = '$_list', `administrateur` = '$_admin', `mail` = '" .
                              $_POST["tech_change_config_mail"] .
                              "' WHERE `technicien`.`id` = '" .
                              $_POST["tech_change_config_id"] .
                              "'"
                      );
                  }
                  mysqli_commit($db_conn);

                  //Ajout modification dans la liste des tickets lundi !
                  //ici
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" />
              <?php
              }

              //Ajout Tech
              if (isset($_POST["tech_create_config"])) {

                  foreach (
                      $_POST["tech_create_config_list_etablissement"]
                      as $_list_add
                  ) {
                      $_list .= "| " . $_list_add . " |";
                  }
                  if (isset($_POST["tech_create_config_admin"])) {
                      $_admin = "1";
                  } else {
                      $_admin = "0";
                  }
                  $hashed = password_hash(
                      $_POST["tech_create_config_mdp"],
                      PASSWORD_DEFAULT
                  );
                  mysqli_query(
                      $db_conn,
                      "INSERT INTO `technicien` (`id`, `utilisateur`, `password`, `administrateur`, `etablissement`, `service`, `mail`, `password-to-change`) VALUES (NULL, '" .
                          $_POST["tech_create_config_nom"] .
                          "', '$hashed', '$_admin', '$_list', '" .
                          $_POST["tech_create_config_service"] .
                          "', '" .
                          $_POST["tech_create_config_mail"] .
                          "', '1')"
                  );
                  mysqli_commit($db_conn);
                  ?>
                <meta http-equiv="refresh" content="0;url=/admin/" />
              <?php
              }

              $result = mysqli_query($db_conn, "SELECT * FROM `technicien` ORDER BY `technicien`.`utilisateur` ASC");

              while ($row = mysqli_fetch_row($result)) {

                  $service_list_exploded = null;
                  $service_list_explode = explode("|", $row[4]);
                  foreach ($service_list_explode as $_) {
                      $service_list_exploded .= $_;
                  }
                  if ($row[3] == "1") {
                      $admin = "👍";
                  } else {
                      $admin = "👎";
                  }
                  echo "<tr>";
                  echo "<td><h4>" . $row[1] . "</h4></th>";
                  echo "<th>" . $admin . "</th>";
                  echo "<th><p>" . $row[6] . "</p></th>";
                  echo "<th><textarea disabled style='resize: none;'>" .
                      $service_list_exploded .
                      "</textarea></th>"; //ici
                  echo "<th><p>" . $row[5] . "</p></th>";
                  echo '<th><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="config_open(`tech_open_' .
                      $row[0] .
                      '`);">🔧</button></th>';
                  echo "</tr>";
                  ?>
                <div id="<?php echo "tech_open_" . $row[0]; ?>" class="modal">
                  <div class="modal-content tech">
                    <div onclick="close_windows('tech_open_<?php echo $row[0]; ?>');"> <span class="close">&times;</span></div>
                    <p><?php
                    echo "";
                    echo "<br><hr class='new1'>";
                    echo "<br>";
                    echo "<div class='tech_open_name'><div class='div_tech_name'>Nom & Prénom : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input class='div_tech_box' type='text' name='tech_change_config_nom' value='$row[1]' required></div>";
                    echo "<br>";
                    echo "<div class='div_tech_eta'>Etablissement : <br>";
                    echo "<select class='tech_select' name='tech_change_config_list_etablissement[]' id='etablissement_list_service' multiple required>";
                    $result__ = mysqli_query(
                        $db_conn,
                        "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                            $_SESSION["username"] .
                            "'"
                    );
                    while ($row__ = mysqli_fetch_row($result__)) {
                        $_list_row = $row__[4];
                    }
                    $result_ = mysqli_query(
                        $db_conn,
                        "SELECT * FROM `etablissement`"
                    );

                    while ($row_ = mysqli_fetch_row($result_)) {
                        $test = strpos($_list_row, "| " . $row_[1] . " |");
                        if ($test !== false) {
                            $etablissemet_service_used = strpos(
                                "| " . $row[4] . " |",
                                $row_[1]
                            );
                            if ($etablissemet_service_used !== false) {
                                echo "<option  selected value='$row_[1]'>$row_[1]</option>";
                            } else {
                                echo "<option   value='$row_[1]'>$row_[1]</option>";
                            }
                        } else {
                            $etablissemet_service_used = strpos(
                                "| " . $row[4] . " |",
                                $row_[1]
                            );
                            if ($etablissemet_service_used !== false) {
                                echo "<option hidden selected value='$row_[1]'>$row_[1]</option>";
                            } else {
                                echo "<option hidden value='$row_[1]'>$row_[1]</option>";
                            }
                        }
                    }
                    echo "</select></div>";
                    echo "<br>";
                    echo "<div class='div_tech_serv'>Service :<br><select class='select_tech' required name='tech_change_config_service'>";
                    echo "<option selected disabled>Prend un service</option>";
                    $result_ = mysqli_query(
                        $db_conn,
                        "SELECT * FROM `service`"
                    );
                    while ($row_ = mysqli_fetch_row($result_)) {
                        if ($row_[1] == $row[5]) {
                            echo "<option selected value='$row_[1]'>$row_[1] ($row_[4])</option>";
                        } else {
                            echo "<option value='$row_[1]'>$row_[1] ($row_[4])</option>";
                        }
                    }
                    echo "</select><br><br></div></div>";
                    echo "<div  class='tech_open_mdp'><div class='div_tech_mdp'>Mot de passe (Optionnel) :<br><input type='password' name='tech_change_config_mdp' class='mdp_tech'></div>";
                    echo "<div class='div_tech_mail'>Mail : <br><input name='tech_change_config_mail' type='text' value='$row[6]' class='mail_tech' required></div>";
                    if ($row[3] == "1") {
                        echo "<br><br><div class='div_tech_admin'>Admin : <input name='tech_change_config_admin' checked type='checkbox' class='admin_check'></div>";
                    } else {
                        echo "<br><br><br><div class='div_tech_admin'>Admin : <input name='tech_change_config_admin' type='checkbox' class='admin_check'></div>";
                    }
                    echo "<input type='text' hidden class='eta_name' name='tech_change_config_id' readonly value='$row[0]'>";
                    echo "<br><br><div class='div_tech_button'><button name='tech_change_config' class='modal_submit' type='submit'>Modifier</button>";
                    echo "</form>";
                    echo "<form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'>";
                    echo "<input type='text' hidden class='eta_name' name='tech_delete_id' readonly value='$row[0]'>";
                    echo "<button name='tech_delete' class='modal_delete' type='submit'>🗑️</button>";
                    echo "</form></div></div>";
                    ?></p>
                  </div>
                </div>
              <?php
              }
              ?>

        </div><!-- page add tech -->
        <div id="tech_create" class="modal">
          <div class="modal-content tech">
            <div onclick="close_windows('tech_create');"> <span class="close">&times;</span></div>
            <p><?php
            echo "";
            echo "<br><hr class='new1'>";
            echo "<br>";
            echo "<div class='tech_open_name'><div class='div_tech_name'>Nom & Prénom : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input class='div_tech_box' type='text' name='tech_create_config_nom' placeholder='ex : (Nard Théo)' required></div>";
            echo "<br>";
            echo "<div class='div_tech_eta'>Etablissement : <br>";
            echo "<select class='tech_select' name='tech_create_config_list_etablissement[]' id='etablissement_list_service' multiple required>";
            //$result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
            //while ($row_ = mysqli_fetch_row($result_)) {
            //echo "<option value='$row_[1]'>$row_[1]</option>";
            //}
            $result__ = mysqli_query(
                $db_conn,
                "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                    $_SESSION["username"] .
                    "'"
            );
            while ($row__ = mysqli_fetch_row($result__)) {
                $_list_row = $row__[4];
            }
            $result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement` ");

            while ($row_ = mysqli_fetch_row($result_)) {
                $test = strpos($_list_row, "| " . $row_[1] . " |");
                if ($test !== false) {
                    echo "<option   value='$row_[1]'>$row_[1]</option>";
                } else {
                    echo "<option hidden value='$row_[1]'>$row_[1]</option>";
                }
            }
            echo "</select></div>";
            echo "<br>";
            echo "<div class='div_tech_serv'>Service :<br><select class='select_tech' required name='tech_create_config_service'>";
            echo "<option selected disabled>Prend un service</option>";
            $result_ = mysqli_query($db_conn, "SELECT * FROM `service`");
            while ($row_ = mysqli_fetch_row($result_)) {
                echo "<option value='$row_[1]'>$row_[1] ($row_[4])</option>";
            }
            echo "</select><br><br></div></div>";
            echo "<div  class='tech_open_mdp'><div class='div_tech_mdp'>Mot de passe :<br><input type='password' name='tech_create_config_mdp' class='mdp_tech' required></div>";
            echo "<br><br><br><div class='div_tech_mail'>Mail : <br><input name='tech_create_config_mail' type='text' placeholder='ex : (info@example.fr)' class='mail_tech' required></div>";
            echo "<br><br><div class='div_tech_admin'>Admin : <input name='tech_create_config_admin' type='checkbox' class='admin_check'></div>";
            echo "<br><br><div class='div_tech_button'><button name='tech_create_config' class='modal_submit' type='submit'>Ajouter</button>";
            echo "</form>";
            echo "</div>";
            ?></p>
          </div>
        </div>

      </div>

      </tbody>
      </table>
    </div>
    <div class="admin_nature">
      <h1>Nature demande :
        <button class="add_button" onclick="config_open(`nature_create`);" type="submit">➕</button>
      </h1>
      <table class="table tablenature">
        <thead>
          <tr>
            <th>Nom</th>
            <th>Etablissement</th>
            <th>Service</th>
            <th>Modifier</th>
          </tr>
        </thead>
        <tbody id="body_1">
          <?php
          if (isset($_POST["nature_delete"])) {
              mysqli_query(
                  $db_conn,
                  "DELETE FROM `nature_demande` WHERE `nature_demande`.`id` = '" .
                      $_POST["nature_delete_id"] .
                      "'"
              );
              mysqli_commit($db_conn);
          }

          //Modification Tech
          if (isset($_POST["nature_change_config"])) {

              foreach (
                  $_POST["nature_change_config_list_etablissement"]
                  as $_list_add
              ) {
                  $_list .= "| " . $_list_add . " |";
              }
              mysqli_query(
                  $db_conn,
                  "UPDATE `nature_demande` SET `nom` = '" .
                      $_POST["nature_change_config_nom"] .
                      "', `permission` = '" .
                      $_POST["nature_change_config_service"] .
                      "', `etablissement` = '$_list' WHERE `nature_demande`.`id` = '" .
                      $_POST["nature_change_config_id"] .
                      "' "
              );
              //mysqli_query($db_conn, "UPDATE `technicien` SET `utilisateur` = '". $_POST{'nature_change_config_nom'} ."', `service` = '". $_POST{'nature_change_config_service'} ."', `etablissement` = '$_list', `administrateur` = '$_admin', `mail` = '".$_POST{'nature_change_config_mail'}."' WHERE `technicien`.`id` = '". $_POST{'nature_change_config_id'} ."'");
              mysqli_commit($db_conn);

              //Ajout modification dans la liste des tickets lundi !
              //ici
              ?>
            <meta http-equiv="refresh" content="0;url=/admin/" />
          <?php
          }

          //Ajout Tech
          if (isset($_POST["nature_create"])) {

              foreach (
                  $_POST["nature_create_list_etablissement"]
                  as $_list_add
              ) {
                  $_list .= "| " . $_list_add . " |";
              }
              mysqli_query(
                  $db_conn,
                  "INSERT INTO `nature_demande` (`id`, `nom`, `permission`, `etablissement`) VALUES (NULL, '" .
                      $_POST["nature_create_nom"] .
                      "', '" .
                      $_POST["nature_create_service"] .
                      "', '$_list') "
              );
              mysqli_commit($db_conn);
              ?>
            <meta http-equiv="refresh" content="0;url=/admin/" />
          <?php
          }

          $result = mysqli_query($db_conn, "SELECT * FROM `nature_demande` ORDER BY `nature_demande`.`nom` ASC");
          while ($row = mysqli_fetch_row($result)) {

              $service_list_exploded = null;
              $service_list_explode = explode("|", $row[3]);
              foreach ($service_list_explode as $_) {
                  $service_list_exploded .= $_;
              }
              echo "<tr>";
              echo "<td><h4>" . $row[1] . "</h4></th>";
              echo "<th>" . $service_list_exploded . "</th>";
              echo "<th>" . $row[2] . "</th>";
              echo '<th><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="config_open(`nature_open_' .
                  $row[0] .
                  '`);">🔧</button></th>';
              echo "</tr>";
              ?>
            <div id="<?php echo "nature_open_" . $row[0]; ?>" class="modal">
              <div class="modal-content tech">
                <div onclick="close_windows('nature_open_<?php echo $row[0]; ?>');"> <span class="close">&times;</span></div>
                <p><?php
                echo "";
                echo "<br><hr class='new1'>";
                echo "<br>";
                echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input type='text' class='nature_box' name='nature_change_config_nom' value='$row[1]' required></h1>";
                echo "<br>";
                echo "<input type='text' hidden class='eta_name' name='nature_change_config_id' readonly value='$row[0]'>";
                echo "<h1>Etablissement : <br>";
                echo "<select class='nature_select' name='nature_change_config_list_etablissement[]' id='etablissement_list_service' multiple required>";

                //$result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
                //while ($row_ = mysqli_fetch_row($result_)) {
                //echo "<option value='$row_[1]'>$row_[1]</option>";
                //}
                $result__ = mysqli_query(
                    $db_conn,
                    "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                        $_SESSION["username"] .
                        "'"
                );
                while ($row__ = mysqli_fetch_row($result__)) {
                    $_list_row = $row__[4];
                }
                $result_ = mysqli_query(
                    $db_conn,
                    "SELECT * FROM `etablissement`"
                );

                while ($row_ = mysqli_fetch_row($result_)) {
                    $test = strpos($_list_row, "| " . $row_[1] . " |");
                    if ($test !== false) {
                        $etablissemet_service_used = strpos(
                            "| " . $row[3] . " |",
                            $row_[1]
                        );
                        if ($etablissemet_service_used !== false) {
                            echo "<option  selected value='$row_[1]'>$row_[1]</option>";
                        } else {
                            echo "<option   value='$row_[1]'>$row_[1]</option>";
                        }
                    } else {
                        $etablissemet_service_used = strpos(
                            "| " . $row[3] . " |",
                            $row_[1]
                        );
                        if ($etablissemet_service_used !== false) {
                            echo "<option hidden selected value='$row_[1]'>$row_[1]</option>";
                        } else {
                            echo "<option hidden value='$row_[1]'>$row_[1]</option>";
                        }
                    }
                }
                echo "</select></h1>";
                echo "<br>";
                echo "<h1>Service :<br><select class='eta_name' required name='nature_change_config_service'>";
                echo "<option selected disabled>Prend un service</option>";
                $result_ = mysqli_query($db_conn, "SELECT * FROM `service`");
                while ($row_ = mysqli_fetch_row($result_)) {
                    if ($row_[1] == $row[2]) {
                        echo "<option selected value='$row_[1]'>$row_[1] ($row_[4])</option>";
                    } else {
                        echo "<option value='$row_[1]'>$row_[1] ($row_[4])</option>";
                    }
                }
                echo "</select></h1>";

                echo "<br><br><button name='nature_change_config' class='modal_submit' type='submit'>Modifier</button>";
                echo "</form>";
                echo "<form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'>";
                echo "<input type='text' hidden name='nature_delete_id' readonly value='$row[0]'>";
                echo "<button name='nature_delete' class='modal_delete' type='submit'>🗑️</button>";
                echo "</form></div>";
                ?></p>
              </div>
            </div>

          <?php
          }
          ?>

    </div><!-- page add tech -->
    <div id="nature_create" class="modal">
      <div class="modal-content tech">
        <div onclick="close_windows('nature_create');"> <span class="close">&times;</span></div>
        <p><?php
        echo "";
        echo "<br><hr class='new1'>";
        echo "<br>";
        echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input type='text' class='nature_box' name='nature_create_nom' value='$row[1]' placeholder='Nom de la nature' required></h1>";
        echo "<br>";
        echo "<input type='text' hidden class='eta_name' name='nature_create_id' readonly value='$row[0]'>";
        echo "<h1>Etablissement : <br>";
        echo "<select class='nature_select' name='nature_create_list_etablissement[]' id='etablissement_list_service' multiple required>";

        //$result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
        //while ($row_ = mysqli_fetch_row($result_)) {
        //echo "<option value='$row_[1]'>$row_[1]</option>";
        //}
        $result__ = mysqli_query(
            $db_conn,
            "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                $_SESSION["username"] .
                "'"
        );
        while ($row__ = mysqli_fetch_row($result__)) {
            $_list_row = $row__[4];
        }
        $result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");

        while ($row_ = mysqli_fetch_row($result_)) {
            $etablissemet_service_used = strpos($_list_row, $row_[1]);
            if ($etablissemet_service_used !== false) {
                echo "<option value='$row_[1]'>$row_[1]</option>";
            } else {
                echo "<option hidden value='$row_[1]'>$row_[1]</option>";
            }
        }

        echo "</select></h1>";
        echo "<br>";
        echo "<h1>Service :<br><select class='select_tech' required name='nature_create_service'>";
        echo "<option selected disabled>Prend un service</option>";
        $result_ = mysqli_query($db_conn, "SELECT * FROM `service`");
        while ($row_ = mysqli_fetch_row($result_)) {
            if ($row_[1] == $row[2]) {
                echo "<option selected value='$row_[1]'>$row_[1] ($row_[4])</option>";
            } else {
                echo "<option value='$row_[1]'>$row_[1] ($row_[4])</option>";
            }
        }
        echo "</select></h1>";

        echo "<br><button name='nature_create' class='modal_submit' type='submit'>Ajouter</button>";
        echo "</form>";
        echo "</div>";
        ?></p>
      </div>
    </div>

    </div>
    </tbody>
    </table>
    </div>
    <div class="admin_demandeur">
      <h1>Service demandeur :
        <button class="add_button" onclick="config_open(`service_demandeur_create`);" type="submit">➕</button>
      </h1>
      <table class="table tabledemandeur">
        <thead>
          <tr>
            <th>Nom</th>
            <th>Etablissement</th>
            <th>Modifier</th>
          </tr>
        </thead>
        <tbody id="body_1">
          <?php
          if (isset($_POST["service_demandeur_delete"])) {
              mysqli_query(
                  $db_conn,
                  "DELETE FROM `service_demandeur` WHERE `service_demandeur`.`id` = '" .
                      $_POST["service_demandeur_delete_id"] .
                      "'"
              );
              mysqli_commit($db_conn);
          }

          //Modification Tech
          if (isset($_POST["service_demandeur_change_config"])) {

              foreach (
                  $_POST["service_demandeur_change_config_list_etablissement"]
                  as $_list_add
              ) {
                  $_list .= "| " . $_list_add . " |";
              }
              mysqli_query(
                  $db_conn,
                  "UPDATE `service_demandeur` SET `couleur` = '".$_POST["service_demandeur_color"]."', `nom` = '" .
                      $_POST["service_demandeur_change_config_nom"] .
                      "',  `etablissement` = '$_list' WHERE `service_demandeur`.`id` = '" .
                      $_POST["service_demandeur_change_config_id"] .
                      "' "
              );
              //mysqli_query($db_conn, "UPDATE `technicien` SET `utilisateur` = '". $_POST{'service_demandeur_change_config_nom'} ."', `service` = '". $_POST{'service_demandeur_change_config_service'} ."', `etablissement` = '$_list', `administrateur` = '$_admin', `mail` = '".$_POST{'service_demandeur_change_config_mail'}."' WHERE `technicien`.`id` = '". $_POST{'service_demandeur_change_config_id'} ."'");
              mysqli_commit($db_conn);

              //Ajout modification dans la liste des tickets lundi !
              //ici
              ?>
            <meta http-equiv="refresh" content="0;url=/admin/" />
          <?php
          }

          //Ajout Tech
          if (isset($_POST["service_demandeur_create"])) {

              foreach (
                  $_POST["service_demandeur_create_list_etablissement"]
                  as $_list_add
              ) {
                  $_list .= "| " . $_list_add . " |";
              }
              mysqli_query(
                  $db_conn,
                  "INSERT INTO `service_demandeur` (`id`, `nom`,  `etablissement`, `couleur`) VALUES (NULL, '" .
                      $_POST["service_demandeur_create_nom"] .
                      "', '$_list', '".$_POST["service_demandeur_color"]."') "
              );
              mysqli_commit($db_conn);
              ?>
            <meta http-equiv="refresh" content="0;url=/admin/" />
          <?php
          }

          $result = mysqli_query($db_conn, "SELECT * FROM `service_demandeur` ORDER BY `service_demandeur`.`nom` ASC");
          while ($row = mysqli_fetch_row($result)) {

              $service_list_exploded = null;
              $service_list_explode = explode("|", $row[2]);
              foreach ($service_list_explode as $_) {
                  $service_list_exploded .= $_;
              }
              echo "<tr>";
              echo "<td><h4>" . $row[1] . "</h4></th>";
              echo "<th>" . $service_list_exploded . "</th>";
              echo '<th><button class="view_button" name="button_view" type="submit" id="button_view" style="text-align: center;" onclick="config_open(`service_demandeur_open_' .
                  $row[0] .
                  '`);">🔧</button></th>';
              echo "</tr>";
              ?>
            <div id="<?php echo "service_demandeur_open_" .
                $row[0]; ?>" class="modal">
              <div class="modal-content tech">
                <div onclick="close_windows('service_demandeur_open_<?php echo $row[0]; ?>');"> <span class="close">&times;</span></div>
                <p><?php
                echo "";
                echo "<br><hr class='new1'>";
                echo "<br>";
                echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input type='text' class='demandeur_box' name='service_demandeur_change_config_nom' value='$row[1]' required></h1>";
                echo "<br>";
                echo "<input type='text' hidden class='eta_name' name='service_demandeur_change_config_id' readonly value='$row[0]'>";
                echo "<h1>Etablissement : <br>";
                echo "<select class='service_demandeur_select' name='service_demandeur_change_config_list_etablissement[]' id='etablissement_list_service' multiple required>";

                //$result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
                //while ($row_ = mysqli_fetch_row($result_)) {
                //echo "<option value='$row_[1]'>$row_[1]</option>";
                //}
                $result__ = mysqli_query(
                    $db_conn,
                    "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                        $_SESSION["username"] .
                        "'"
                );
                while ($row__ = mysqli_fetch_row($result__)) {
                    $_list_row = $row__[4];
                }
                $result_ = mysqli_query(
                    $db_conn,
                    "SELECT * FROM `etablissement`"
                );

                while ($row_ = mysqli_fetch_row($result_)) {
                    $test = strpos($_list_row, "| " . $row_[1] . " |");
                    if ($test !== false) {
                        $etablissemet_service_used = strpos(
                            "| " . $row[2] . " |",
                            $row_[1]
                        );
                        if ($etablissemet_service_used !== false) {
                            echo "<option  selected value='$row_[1]'>$row_[1]</option>";
                        } else {
                            echo "<option   value='$row_[1]'>$row_[1]</option>";
                        }
                    } else {
                        $etablissemet_service_used = strpos(
                            "| " . $row[2] . " |",
                            $row_[1]
                        );
                        if ($etablissemet_service_used !== false) {
                            echo "<option hidden selected value='$row_[1]'>$row_[1]</option>";
                        } else {
                            echo "<option hidden value='$row_[1]'>$row_[1]</option>";
                        }
                    }
                }
                echo "</select></h1>";
                echo "<br>";
                echo "<h1>Couleur (Héxa) :";
                echo "<br>";
                echo "<input type='text' name='service_demandeur_color' class='eta_name' placeholder='ex : #ffffff ou rien' value='$row[3]'> </h1>";
                echo "<br><br>";
                echo "<button name='service_demandeur_change_config' class='modal_submit' type='submit'>Modifier</button>";
                echo "</form>";
                echo "<form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'>";
                echo "<input type='text' hidden name='service_demandeur_delete_id' readonly value='$row[0]'>";
                echo "<button name='service_demandeur_delete' class='modal_delete' type='submit'>🗑️</button>";
                echo "</form></div>";
                ?></p>
              </div>
            </div>

          <?php
          }
          ?>

    </div><!-- page add tech -->
    <div id="service_demandeur_create" class="modal">
      <div class="modal-content tech">
        <div onclick="close_windows('service_demandeur_create');"> <span class="close">&times;</span></div>
        <p><?php
        echo "";
        echo "<br><hr class='new1'>";
        echo "<br>";
        echo "<h1>Dénomination : <form action=' ' method='post' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?'><input type='text' class='demandeur_box'name='service_demandeur_create_nom' value='$row[1]' placeholder='Nom du service' required></h1>";
        echo "<br>";
        echo "<input type='text' hidden class='eta_name' name='service_demandeur_create_id' readonly value='$row[0]'>";
        echo "<h1>Etablissement : <br>";
        echo "<select class='service_demandeur_select' name='service_demandeur_create_list_etablissement[]' id='etablissement_list_service' multiple required>";

        //$result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
        //while ($row_ = mysqli_fetch_row($result_)) {
        //echo "<option value='$row_[1]'>$row_[1]</option>";
        //}
        $result__ = mysqli_query(
            $db_conn,
            "SELECT * FROM `technicien` WHERE `utilisateur` = '" .
                $_SESSION["username"] .
                "'"
        );
        while ($row__ = mysqli_fetch_row($result__)) {
            $_list_row = $row__[4];
        }
        $result_ = mysqli_query($db_conn, "SELECT * FROM `etablissement`");

        while ($row_ = mysqli_fetch_row($result_)) {
            $etablissemet_service_used = strpos($_list_row, $row_[1]);
            if ($etablissemet_service_used !== false) {
                echo "<option value='$row_[1]'>$row_[1]</option>";
            } else {
                echo "<option hidden value='$row_[1]'>$row_[1]</option>";
            }
        }

        echo "</select></h1>";
        echo "<br>";
        echo "<h1>Couleur (Héxa) :";
        echo "<br>";
        echo "<input type='text' name='service_demandeur_color' class='eta_name' placeholder='ex : #ffffff ou rien' value='$row[3]'> </h1>";
        echo "<br><br>";
        echo "<button name='service_demandeur_create' class='modal_submit' type='submit'>Ajouter</button>";
        echo "</form>";
        echo "</div>";
        ?></p>
      </div>
    </div>

    </div>

    </tbody>
    </table>
    </div>

    </div>

    </div>


    </div>
  <?php } ?>

  <?php if ($div["admin_login"] == 1) {
      if (isset($_POST["identifiant"])) {
          $email = $_POST["identifiant"];
          $pass = $_POST["mdp"];
          $result = mysqli_query(
              $db_conn,
              "SELECT * FROM `technicien` WHERE mail = '$email'"
          );

          while ($row = mysqli_fetch_row($result)) {
              //echo "Database : ". $row[2] . "<br>";
              //echo password_hash($pass, PASSWORD_DEFAULT) . "<br>";
              if (password_verify($pass, $row[2])) {
                  $_SESSION["authenticated"] = 1;
                  $_SESSION["username"] = $row[1];
                  $_SESSION["admin"] = $row[3];
                  header("location: /admin/");
                  //echo "C good";
                  //header("Location: /admin/");
              }
          }
      } ?>

    <head>
      <meta charset="utf-8">
      <title>Centre hospitalier intercommunal Monts &amp; Barrages</title>
      <link rel="stylesheet" href="/css/login.css">
    </head>

    <div class="center">
      <h1>Identification</h1>
      <form action=" " method="post">
        <div class="txt_field">
          <input type="text" name="identifiant" required>
          <span></span>
          <label>E-Mail</label>
        </div>
        <div class="txt_field">
          <input type="password" name="mdp" required>
          <span></span>
          <label>Mot de passe</label>
        </div>
        <input type="submit" value="Login">
        <br><br>
        <?php if (isset($user)) { ?>
          <div class="alert">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Le mot de passe ou l'identifiant n'est pas correct.
          </div>
          <br>
        <?php } ?>
      </form>
    </div>

  <?php
  } ?>


  <?php if (
      $div["intervention_ticket_view"] == 1
  ) { ?> <!-- Affichage des tickets -->
    <div class="service">
      <a href="/"><img src="<?php echo $etablissement_logo; ?>" width="10%" height="10%"></a>
    </div>
    <div class="ticket_view_box">
    <!-- Backpage : <input type="button" onclick="window.location.replace('../');" /> -->
    <?php
    if(isset($_POST["ticket_view_transfer"])){
      mysqli_query($db_conn, "UPDATE `intervention` SET `service` = '".$_POST["ticket_view_transfer_nom"]."' WHERE `intervention`.`id` = '".$_POST["ticket_view_id"] ."'");
      mysqli_commit($db_conn);
      ?>
      <meta http-equiv="refresh" content="0;url=/<?php echo "$etablissement_used/$service_used";?>" />
      <?php
    }
    if (
        isset($_POST["ticket_view_recuperation"]) &&
        $_POST["ticket_view_new_statut"] == 1
    ) {
      $_query_result = mysqli_query( //Récupération mail du service
        $db_conn,
        "SELECT * FROM `service` WHERE `etablissement` LIKE '%| $etablissement_used |%' AND `nom` LIKE '$service_used'"
      );
      while ($_query_row = mysqli_fetch_row($_query_result)) {
          $mail_service = $_query_row[3];
      }
      try { //Envoi ticket quand récupération
        $mail = new PHPMailer(true);
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->addAddress($mail_service);
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER; //log
        $mail->isSMTP();
        $mail->Host       = 'smtp1.sil.fr';
        $mail->SMTPAuth   = false;
        $mail->Username   = 'user@example.com';
        $mail->Password   = 'secret';
        $mail->Port       = 25;
        
        $mail->CharSet = "UTF-8";
        $actual_link = "http://$_SERVER[HTTP_HOST]/$etablissement_used/$service_used/view/".$_POST["ticket_view_id"]."/";
        $mail->setFrom($mail_service, "Bon d'intervention");
        $mail->isHTML(true);
        $mail->Subject = "Ticket pris en charge - ". $_POST["ticket_view_date_prise_en_charge"];
        $mail->Body    = "<p style='text-align: center;'><span style='text-decoration: underline;'><strong>ID :</strong></span> <em><a href='$actual_link'>".$_POST["ticket_view_id"]."</a></em></p>
        <p style='text-align: center;'><span style='text-decoration: underline;'><strong>Nature :</strong></span> <em>".$_POST["ticket_view_nature"]."</em></p>
        <p style='text-align: center;'><strong><span style='text-decoration: underline;'>Technicien :</span></strong> <em>".$_POST["ticket_view_tech"]."</em></p>
        <p style='text-align: center;'>&nbsp;</p>
        <p style='text-align: center;'>&nbsp;</p>
        <p style='text-align: center;'>&nbsp;</p>
        <p style='text-align: center;'>-------------------------------------------------------------------------</p>
        <p style='text-align: center;'>Gestionnaire de ticket</p>
        <p style='text-align: center;'>-------------------------------------------------------------------------</p>";
        $mail->Send();
      } catch (Exception $e) {
          echo 'Exception reçue : ',  $e->getMessage(), "\n";
      }
        mysqli_query(
            $db_conn,
            "UPDATE `intervention` SET `nom_technicien` = '" .
                $_POST["ticket_view_tech"] .
                "', `nature_demande` = '" .
                $_POST["ticket_view_nature"] .
                "', `statut` = '" .
                $_POST["ticket_view_new_statut"] .
                "', `date_prise_en_charge` = '" .
                $_POST["ticket_view_date_prise_en_charge"] .
                "' WHERE `intervention`.`id` = '" .
                $_POST["ticket_view_id"] .
                "'"
        );
        mysqli_commit($db_conn);
    } elseif (isset($_POST["ticket_view_recuperation"])) {
        //echo "UPDATE `intervention` SET `date_fin` = '".$_POST['ticket_view_date_fin']."', `remarque` = '".$_POST['ticket_view_remarque']."' WHERE `intervention`.`id` = '".$_POST['ticket_view_id']."'";
        mysqli_query(
            $db_conn,
            "UPDATE `intervention` SET `date_fin` = '" .
                $_POST["ticket_view_date_fin"] .
                "', `remarque` = '" .
                mysqli_real_escape_string($db_conn, $_POST["ticket_view_remarque"]) .
                "' WHERE `intervention`.`id` = '" .
                $_POST["ticket_view_id"] .
                "'"
        );
        mysqli_commit($db_conn);
    }
    if (isset($_POST["ticket_view_closeit"])) {
        mysqli_query(
            $db_conn,
            "UPDATE `intervention` SET `statut` = '2' WHERE `intervention`.`id` = '" .
                $_POST["ticket_view_id"] .
                "'"
        );
        mysqli_commit($db_conn);
        $_query_result = mysqli_query( //Récupération mail du service
          $db_conn,
          "SELECT * FROM `service` WHERE `etablissement` LIKE '%| $etablissement_used |%' AND `nom` LIKE '$service_used'"
        );
        while ($_query_row = mysqli_fetch_row($_query_result)) {
            $mail_service = $_query_row[3];
        }
        try { //Envoie ticket cloturation
          $mail = new PHPMailer(true);
          $mail->SMTPOptions = array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
              )
          );
          $mail->addAddress($mail_service);
          //$mail->SMTPDebug = SMTP::DEBUG_SERVER; //log
          $mail->isSMTP();
          $mail->Host       = 'smtp1.sil.fr';
          $mail->SMTPAuth   = false;
          $mail->Username   = 'user@example.com';
          $mail->Password   = 'secret';
          $mail->Port       = 25;
          
          $dates = new DateTime();
          $request_date = $dates->format("Y-m-d H:i:s");
          $mail->CharSet = "UTF-8";
          $actual_link = "http://$_SERVER[HTTP_HOST]/$etablissement_used/$service_used/view/".$_POST["ticket_view_id"]."/";
          $mail->setFrom($mail_service, "Bon d'intervention");
          $mail->isHTML(true);
          $mail->Subject = "Ticket cloturé - $request_date";
          $mail->Body    = "<p style='text-align: center;'><span style='text-decoration: underline;'><strong>ID :</strong></span> <em><a href='$actual_link'>".$_POST["ticket_view_id"]."</a></em></p>
          <p style='text-align: center;'>&nbsp;</p>
          <p style='text-align: center;'>&nbsp;</p>
          <p style='text-align: center;'>&nbsp;</p>
          <p style='text-align: center;'>-------------------------------------------------------------------------</p>
          <p style='text-align: center;'>Gestionnaire de ticket</p>
          <p style='text-align: center;'>-------------------------------------------------------------------------</p>";
          $mail->Send();
        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
    }

    //
    $result = mysqli_query($db_conn, "SELECT * FROM statut");
    while ($row = mysqli_fetch_array($result)) {
        $statut_name[$row[0]] = $row[1];
    }
    $result = mysqli_query(
        $db_conn,
        "SELECT * FROM `intervention` WHERE `id` = '$url_parse[3]'"
    );

    while ($row = mysqli_fetch_row($result)) {
      $_statut_page = 1;
      if($row[3] == $service_used && $row[1] == $etablissement_used){
        $dates = new DateTime();
        $_demandeur = $row[5];
        $_service_demandeur = $row[4];
        $_date_creation = $row[2];
        $_tech = $row[6];
        $_nature = $row[7];
        $_date_prise = $row[10];
        $_date_fin = $row[11];
        $_detail = $row[8];
        $_remarque = $row[12];
        if($row[9] == 0 && $_SESSION["authenticated"]){
          echo "<form action='' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?' method='post'>";
          echo "<input type='text' hidden name='ticket_view_id' class='ticket_view_id' readonly value='$row[0]'>";
          echo "<select class='ticket_button_change' name='ticket_view_transfer_nom' required>";
          $result_ = mysqli_query($db_conn, "SELECT * FROM service WHERE etablissement like '%| $etablissement_used |%'");
          while ($row_ = mysqli_fetch_row($result_)) {
              if ($service_used == $row_[1]) {
                  echo "<option disabled selected value='$row_[1]'>$row_[1]</option>";
              } else {
                  echo "<option value='$row_[1]'>$row_[1]</option>";
              }
          }
          echo "</select>";
          echo "<button class='ticket_button_transfer' name='ticket_view_transfer' type='submit'>Transferer</button>";
        }
        echo "</form>";
        echo "<form action='' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?' method='post'>";
        echo "<h1>Visualisation du bon : <input type='text' name='ticket_view_id' class='ticket_view_id' readonly value='$row[0]'>";
        //if( $_SESSION["authenticated"]) echo "<select class='intervention_view_status' name='ticket_view_statut_id' required>";
        //else
        echo "<select disabled class='intervention_view_statut' name='ticket_view_statut_id' required>";
        $result_ = mysqli_query($db_conn, "SELECT * FROM statut");
        while ($row_ = mysqli_fetch_row($result_)) {
            if ($row[9] == $row_[0]) {
                echo "<option selected value='$row_[0]'>$row_[1]</option>";
            } else {
                echo "<option value='$row_[0]'>$row_[1]</option>";
            }
        }
        echo "</select>";
        
        /*echo "<select hidden name='ticket_view_statut' class='intervention_view_status' required>";
        $result_ = mysqli_query($db_conn, "SELECT * FROM statut");
        while ($row_ = mysqli_fetch_row($result_)) {
        if ($row[9] == $row_[0]) {
          echo "<option selected value='$row_[0]'>$row_[1]</option>";
        } else {
          echo "<option value='$row_[0]'>$row_[1]</option>";
        }
        }
        echo "</select>";*/
        echo "</h1><hr class='line_ticket'>";
        echo "<div class='intervention_view_client'><h3>Demandeur :<input type='text' class='ticket_view_demandeur' readonly value='$_demandeur'></h3><br>";
        echo "<h3>Service :<input type='text' class='ticket_view_serv' readonly value='$_service_demandeur'></h3><br>";
        echo "<h3>Date création :<input type='text' class='ticket_view_creation' readonly value='$_date_creation'></h3><br>";
        if ($row[13] == "1") {
            echo "<h3>Voir avec le personnel personnel :<input checked disabled type='checkbox' value='' class='intervention_view_perso'></h3></div>";
        } else {
            echo "<h3>Voir avec le personnel personnel :<input checked disabled type='checkbox' value='' class='intervention_view_perso'></h3></div>";
        }

        if ($_SESSION["authenticated"]) {
              $__result = mysqli_query(
              $db_conn,
              "SELECT * FROM `technicien` WHERE utilisateur = '" .
                  $_SESSION["username"] .
                  "'"
              );

              while ($__row = mysqli_fetch_row($__result)) {
                  $perm_service = $__row[5];
                  $perm_etablissement_list .= $__row[4];
              }
              
              $perm_etablissement = strpos( $perm_etablissement_list, "| " . $row[1] . " |");
            if ($row[9] == 0 && $perm_etablissement !== false && $perm_service == $row[3]) {
                if ($_date_fin == "") {
                    $_date_fin = $dates->format("Y-m-d H:i");
                }
                if ($_date_prise == "") {
                    $_date_prise = $dates->format("Y-m-d H:i");
                }
                if ($_tech == "") {
                    $_tech = $_SESSION["username"];
                }
                echo "<div class='take_ticket_tech'><h3>Technicien :<input type='text' name='ticket_view_tech' class='ticket_view_tech' value='$_tech' required></h3><br>";
                echo "<h3>Nature demande :<select name='ticket_view_nature' class='intervention_take_nature' id='intervention_take_nature' required>";
                echo "<option selected disabled>Prend un truc</option>";
                $result_ = mysqli_query(
                    $db_conn,
                    "SELECT * FROM nature_demande WHERE `etablissement` LIKE '%| $etablissement_used |%' AND `permission` LIKE '%$service_used%'"
                );
                while ($row_ = mysqli_fetch_row($result_)) {
                    if ($row[7] == $row_[1]) {
                        echo "<option selected value='$row_[1]'>$row_[1]</option>";
                    } else {
                        echo "<option value='$row_[1]'>$row_[1]</option>";
                    }
                }
                echo "</select></h3>";
                echo "<h3>Date prise en charge :<input type='text' class='ticket_take_charge' name='ticket_view_date_prise_en_charge' value='$_date_prise' required></h3><br>";
                //echo "<h3>Date de fin :<input type='text' class='ticket_take_fin' readonly value='-'></h3>";
                echo "</div><br><div class='ticket_view_detail'><h3>Details :</h3><textarea class='intervention_view_detail' rows='5' readonly placeholder='Details'>$_detail</textarea></div>";
                echo "<div class='ticket_view_remarque'><h3>Remarque :</h3><textarea class='intervention_view_remarque' rows='5' readonly placeholder='Remarque'>$_remarque</textarea></div>";
                echo "<input type='text' name='ticket_view_new_statut' hidden class='ticket_view_id' readonly value='1'>";
                echo "<button name='ticket_view_recuperation' class='ticket_take_bon' type='submit'>Prendre en charge le ticket</button>";
                echo "</form>";
            } elseif ($row[9] == 1 && $_tech == $_SESSION["username"]) {
                if ($_date_fin == "") {
                    $_date_fin = $dates->format("Y-m-d H:i");
                }
                if ($_date_prise == "") {
                    $_date_prise = $dates->format("Y-m-d H:i");
                }
                if ($_tech == "") {
                    $_tech = $_SESSION["username"];
                }
                echo "<div class='intervention_ticket_tech'><div class='ticket_fin_tech'><h3>Technicien :<input type='text' name='ticket_view_tech' readonly class='ticket_view_creation' value='$_tech'></h3>";
                echo "<h3>Nature de la demande :<input type='text' class='ticket_view_nature' name='ticket_view_nature' readonly value='$_nature'></h3>";
                echo "<h3>Date prise en charge :<input type='text' class='ticket_view_charge' name='ticket_view_date_prise_en_charge' readonly value='$_date_prise'></h3></div>";
                echo "<h3>Date de fin :<input type='text' class='ticket_fin_fin' name='ticket_view_date_fin' required value='$_date_fin'></h3></div>";
                echo "<br><div class='ticket_view_detail'><h3>Details :</h3><textarea class='intervention_view_detail' readonly rows='5' placeholder='Details'>$_detail</textarea></div>";
                echo "<div class='ticket_view_remarque'><h3>Remarque :</h3><textarea name='ticket_view_remarque' class='intervention_view_remarque' rows='5' placeholder='Remarque' required>$_remarque</textarea></div>";
                echo "<div class='ticket_view_button_box_1'><button class='ticket_view_modif' name='ticket_view_recuperation' type='submit'>Sauvegarder</button></div>";
                echo "</form>";
                echo "<div class='ticket_view_button_box_1'><form action='' data-confirm='Êtes-vous sur de vouloir cliquer sur ce bouton ?' method='post'>";
                echo "<input type='text' hidden name='ticket_view_id' class='ticket_view_id' readonly value='$row[0]'>";
                echo "<button class='ticket_view_cloture' name='ticket_view_closeit' type='submit'>Cloturer (Sans sauvegarde)</button></div>";
                echo "</form>";
                //echo "Terminé : <input type='checkbox' name='ticket_view_closeit' class='intervention_view_perso'>";
            } else {
                echo "<div class='intervention_view_ticket'><h3>Technicien :<input type='text' name='ticket_view_tech' class='ticket_view_creation' readonly value='$_tech'></h3><br>";
                echo "<h3>Nature de la demande :<input type='text' class='ticket_view_nature' name='ticket_view_nature' readonly value='$_nature'></h3><br>";
                echo "<h3>Date prise en charge :<input type='text' class='ticket_view_charge' name='ticket_view_date_prise_en_charge' readonly value='$_date_prise'></h3><br>";
                echo "<h3>Date de fin :<input type='text' class='ticket_view_charge' readonly value='$_date_fin'></h3></div>";
                echo "<br><div class='ticket_view_detail'><h3>Details :</h3><textarea readonly class='intervention_view_detail' rows='5' placeholder='Details'>$_detail</textarea></div>";
                echo "<div class='ticket_view_remarque'><h3>Remarque :</h3><textarea readonly class='intervention_view_remarque' rows='5' placeholder='Remarque'>$_remarque</textarea></div>";
                echo "</form>";
            }
        } else {
            echo "<div class='intervention_view_ticket'><h3>Technicien :<input type='text' name='ticket_view_tech' class='ticket_view_creation' readonly value='$_tech'></h3><br>";
            echo "<h3>Nature de la demande :<input type='text' class='ticket_view_nature' name='ticket_view_nature' readonly value='$_nature'></h3><br>";
            echo "<h3>Date prise en charge :<input type='text' class='ticket_view_charge' name='ticket_view_date_prise_en_charge' readonly value='$_date_prise'></h3><br>";
            echo "<h3>Date de fin :<input type='text' class='ticket_view_charge' readonly value='$_date_fin'></h3></div>";
            echo "<br><div class='ticket_view_detail'><h3>Details :</h3><textarea readonly class='intervention_view_detail' rows='5' placeholder='Details'>$_detail</textarea></div>";
            echo "<div class='ticket_view_remarque'><h3>Remarque :</h3><textarea readonly class='intervention_view_remarque' rows='5' placeholder='Remarque'>$_remarque</textarea></div>";
            echo "</form>";
        }
    }else{
    
      ?>
      <meta http-equiv="refresh" content="0;url=/<?php echo "$etablissement_used/$service_used/view/";?>" />
      <?php
    
    }
  }
  if($_statut_page == 0){
    
    ?>
    <meta http-equiv="refresh" content="0;url=/<?php echo "$etablissement_used/$service_used/view/";?>" />
    <?php
  }
    ?>
    </div>
  <?php } ?>
  <?php if ($div["stats"] == 1) { ?>
    <div class="stats">
    </div>

    <?php
$dates = new DateTime();
$default_date = $dates->format("Y");
if($_POST["stat_refresh_etablissement"] == "tous") $default_etablissement = "";
else if(isset($_POST["stat_refresh_etablissement"])){
   $default_etablissement = "AND `etablissement` = '" . $_POST["stat_refresh_etablissement"] . "'";
   $default_etablissement_v2 = "AND `etablissement` LIKE '%| " . $_POST["stat_refresh_etablissement"] . " |%'";
}
if($_POST["stat_refresh_service"]== "tous") $default_service = "";
else if(isset($_POST["stat_refresh_service"])) {
  $default_service = "AND `service` = '" . $_POST["stat_refresh_service"] ."'";
  $default_service_v2 = "AND `service` = '" . $_POST["stat_refresh_service"] ."'";
  $default_permission_v2 = "AND `permission` = '" . $_POST["stat_refresh_service"] ."'";
}
if(isset($_POST["stat_date"])) $default_date = $_POST["stat_date"];
for($i = 1; $i < 13; $i++){ //Total par an
  if($i < 10) $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-0$i-%' $default_etablissement $default_service");
  else $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-$i-%' $default_etablissement  $default_service");
  $graph_date_total[$i] = mysqli_num_rows($result);
  $total_par_an += $graph_date_total[$i];
}
for($i = 1; $i < 13; $i++){ //Total par an
  if($i < 10) $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-0$i-%' $default_etablissement $default_service AND `statut` = 0");
  else $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-$i-%' $default_etablissement  $default_service AND `statut` = 0");
  $graph_date_total_1[$i] = mysqli_num_rows($result);
  $total_par_an_2 += $graph_date_total_1[$i];
}
for($i = 1; $i < 13; $i++){ //Total par an
  if($i < 10) $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-0$i-%' $default_etablissement $default_service AND `statut` = 1");
  else $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-$i-%' $default_etablissement  $default_service AND `statut` = 1");
  $graph_date_total_2[$i] = mysqli_num_rows($result);
  $total_par_an_2 += $graph_date_total_2[$i];
}
for($i = 1; $i < 13; $i++){ //Total par an
  if($i < 10) $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-0$i-%' $default_etablissement $default_service AND `statut` = 2");
  else $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-$i-%' $default_etablissement  $default_service AND `statut` = 2");
  $graph_date_total_3[$i] = mysqli_num_rows($result);
  $total_par_an_3 += $graph_date_total_3[$i];
}

$_query_result = mysqli_query($db_conn, "SELECT * FROM `service_demandeur` WHERE 1 $default_etablissement_v2 ORDER BY nom ASC"); //Total service demandeur
$i = 0;
while ($_query_row = mysqli_fetch_row($_query_result)) {
  $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-%' $default_etablissement $default_service AND `service_demandeur` = '$_query_row[1]'");
  $graph_service_demandeur[$i] = mysqli_num_rows($result);
  $graph_service_demandeur_nom[$i] = $_query_row[1];
  $total_par_service_demandeur += $graph_service_demandeur[$i];
  $i++;
}

$_query_result = mysqli_query($db_conn, "SELECT * FROM `nature_demande` WHERE 1 $default_etablissement_v2 $default_permission_v2 ORDER BY nom ASC"); //Total par nature demande
$i = 0;
while ($_query_row = mysqli_fetch_row($_query_result)) {
  $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-%' $default_etablissement $default_service AND `nature_demande` = '$_query_row[1]'");
  $graph_nature_demande[$i] = mysqli_num_rows($result);
  $graph_nature_demande_nom[$i] = $_query_row[1];
  $total_par_nature_demande += $graph_nature_demande[$i];
  $i++;
}

$_query_result = mysqli_query($db_conn, "SELECT * FROM `technicien` WHERE 1 $default_etablissement_v2 $default_service_v2 ORDER BY utilisateur ASC"); //Total par tech
$i = 0;
while ($_query_row = mysqli_fetch_row($_query_result)) {
  $result = mysqli_query($db_conn, "SELECT statut FROM intervention WHERE `date_demande` like '%$default_date-%' $default_etablissement $default_service AND `nom_technicien` = '$_query_row[1]'");
  $graph_tech[$i] = mysqli_num_rows($result);
  $graph_tech_nom[$i] = $_query_row[1];
  $total_par_tech += $graph_tech[$i];
  $i++;
}



//if($i < 10) $result = mysqli_query($db_conn, "SELECT id FROM intervention WHERE `date_demande` like '%2021-0$i%'");
//else $result = mysqli_query($db_conn, "SELECT id FROM intervention WHERE `date_demande` like '%2021-$i%'");
$data1 = array( //Total par an
	array("y" => $graph_date_total[1], "label" => "janv."),
	array("y" => $graph_date_total[2], "label" => "févr."),
	array("y" => $graph_date_total[3], "label" => "mars"),
	array("y" => $graph_date_total[4], "label" => "avr."),
	array("y" => $graph_date_total[5], "label" => "mai"),
	array("y" => $graph_date_total[6], "label" => "juin"),
	array("y" => $graph_date_total[7], "label" => "juil."),
	array("y" => $graph_date_total[8], "label" => "août"),
	array("y" => $graph_date_total[9], "label" => "sept."),
	array("y" => $graph_date_total[10], "label" => "oct."),
	array("y" => $graph_date_total[11], "label" => "nov."),
	array("y" => $graph_date_total[12], "label" => "déc.")
);
$data1_1 = array( //Total par an
	array("y" => $graph_date_total_1[1], "label" => "janv."),
	array("y" => $graph_date_total_1[2], "label" => "févr."),
	array("y" => $graph_date_total_1[3], "label" => "mars"),
	array("y" => $graph_date_total_1[4], "label" => "avr."),
	array("y" => $graph_date_total_1[5], "label" => "mai"),
	array("y" => $graph_date_total_1[6], "label" => "juin"),
	array("y" => $graph_date_total_1[7], "label" => "juil."),
	array("y" => $graph_date_total_1[8], "label" => "août"),
	array("y" => $graph_date_total_1[9], "label" => "sept."),
	array("y" => $graph_date_total_1[10], "label" => "oct."),
	array("y" => $graph_date_total_1[11], "label" => "nov."),
	array("y" => $graph_date_total_1[12], "label" => "déc.")
);
$data1_2 = array( //Total par an
	array("y" => $graph_date_total_2[1], "label" => "janv."),
	array("y" => $graph_date_total_2[2], "label" => "févr."),
	array("y" => $graph_date_total_2[3], "label" => "mars"),
	array("y" => $graph_date_total_2[4], "label" => "avr."),
	array("y" => $graph_date_total_2[5], "label" => "mai"),
	array("y" => $graph_date_total_2[6], "label" => "juin"),
	array("y" => $graph_date_total_2[7], "label" => "juil."),
	array("y" => $graph_date_total_2[8], "label" => "août"),
	array("y" => $graph_date_total_2[9], "label" => "sept."),
	array("y" => $graph_date_total_2[10], "label" => "oct."),
	array("y" => $graph_date_total_2[11], "label" => "nov."),
	array("y" => $graph_date_total_2[12], "label" => "déc.")
);
$data1_3 = array( //Total par an
	array("y" => $graph_date_total_3[1], "label" => "janv."),
	array("y" => $graph_date_total_3[2], "label" => "févr."),
	array("y" => $graph_date_total_3[3], "label" => "mars"),
	array("y" => $graph_date_total_3[4], "label" => "avr."),
	array("y" => $graph_date_total_3[5], "label" => "mai"),
	array("y" => $graph_date_total_3[6], "label" => "juin"),
	array("y" => $graph_date_total_3[7], "label" => "juil."),
	array("y" => $graph_date_total_3[8], "label" => "août"),
	array("y" => $graph_date_total_3[9], "label" => "sept."),
	array("y" => $graph_date_total_3[10], "label" => "oct."),
	array("y" => $graph_date_total_3[11], "label" => "nov."),
	array("y" => $graph_date_total_3[12], "label" => "déc.")
);

$data2 = array( //Total par service demandeur
);

$i = 0;
foreach($graph_service_demandeur as $graph_service_value){
  $data2[] = array("y" => $graph_service_value, "label" => $graph_service_demandeur_nom[$i]);
  $i++;
}
$data2[] = array("y" => ($total_par_an -  $total_par_service_demandeur), "label" => "Autre");


$data3 = array( //Total par nature demande
);

$i = 0;
foreach($graph_nature_demande as $graph_nature_demande_value){
  $data3[] = array("y" => $graph_nature_demande_value, "label" => $graph_nature_demande_nom[$i]);
  $i++;
}
$data3[] = array("y" => ($total_par_an - $total_par_nature_demande ), "label" => "Autre");


$data4 = array( //Total par technicien
);
$i = 0;
foreach($graph_tech as $graph_tech_value){
  $data4[] = array("y" => $graph_tech_value, "label" => $graph_tech_nom[$i]);
  $i++;
}
$data4[] = array("y" => ($total_par_an - $total_par_tech), "label" => "Autre");



?>
<head>
<script> //Affichage des graph
window.onload = function () {
 
  var chart_1 = new CanvasJS.Chart("chart1", {
    theme: "dark2",
    zoomEnabled: true,
    exportEnabled: true,
    animationEnabled: true,
    toolTip:{
      shared: true
    },
    legend:{
      cursor: "pointer",
      fontSize: 16,
      itemclick: toggleDataSeries
    },
    title: {
      text: "(Total : <?php echo $total_par_an;?>) - <?php echo $default_date; ?>"
    },
    axisY: {
      //title: "Number of Push-ups"
    },
    axisX: {
      //Color: "red",
      
     
    },
    
    data: [{
      color: "rgba(255,255,255,.5)",
      type: "stackedColumn", //splineArea
      showInLegend: true,
      name: "Total",
      visible: false,
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data1, JSON_NUMERIC_CHECK); ?>
    },
    {
      color: "rgba(255,0,0,.5)",
      type: "stackedColumn",
      showInLegend: true,
      name: "En attente",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data1_1, JSON_NUMERIC_CHECK); ?>
    },
    {
      color: "rgba(255,255,0,.5)",
      type: "stackedColumn",
      showInLegend: true,
      name: "En cours",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data1_2, JSON_NUMERIC_CHECK); ?>
    },
    {
      color: "rgba(0,255,0,.5)",
      type: "stackedColumn",
      showInLegend: true,
      name: "Terminer",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data1_3, JSON_NUMERIC_CHECK); ?>
    }
  ]
  });

  chart_1.render();

  var chart = new CanvasJS.Chart("chart2", {
    theme: "dark2",
    zoomEnabled: true,
    exportEnabled: true,
    animationEnabled: true,
    title: {
      text: "(Total : <?php echo $total_par_service_demandeur ;?>) - <?php echo $default_date; ?>"
    },
    axisY: {
      //title: "Number of Push-ups"
    },
    axisX: {
      //Color: "red",
    },
    data: [{
      color: "rgba(255,255,255,.5)",
      type: "column",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data2, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render();
  
  var chart = new CanvasJS.Chart("chart3", {
    theme: "dark2",
    zoomEnabled: true,
    exportEnabled: true,
    animationEnabled: true,
    title: {
      text: "(Total : <?php echo $total_par_nature_demande;?>) - <?php echo $default_date; ?>"
    },
    axisY: {
      //title: "Number of Push-ups"
    },
    axisX: {
      //Color: "red",
    },
    data: [{
      color: "rgba(255,255,255,.5)",
      type: "column",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data3, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render();

  var chart = new CanvasJS.Chart("chart4", {
    theme: "dark2",
    zoomEnabled: true,
    exportEnabled: true,
    animationEnabled: true,
    title: {
      text: "(Total : <?php echo $total_par_tech;?>) - <?php echo $default_date; ?>"
    },
    axisY: {
      //title: "Number of Push-ups"
    },
    axisX: {
      //Color: "red",
    },
    data: [{
      color: "rgba(255,255,255,.5)",
      type: "column",
      yValueType: "dateTime",
      dataPoints: <?php echo json_encode($data4, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render();

  function toggleDataSeries(e){
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
      e.dataSeries.visible = false;
    }
    else{
      e.dataSeries.visible = true;
    }
    chart_1.render();
  }
}
</script>
</head>
<body>
  <div class="stat_intervention">
  <h1>Statistiques par nombres d'intervention par an :</h1>
<div id="chart1" class="chart chart1"></div>
<br></div>
  <div class="stat_demandeur">
  <h1>Statistiques par service demandeur :</h1>
<div id="chart2" class="chart chart2" ></div><br>
</div><div class="stat_panne">
<h1>Statistiques par type de panne :</h1>
<div id="chart3" class="chart chart3"></div>
</div><div class="stat_tech">
<h1>Statistiques par technicien :</h1>
<div id="chart4" class="chart chart4"></div>
</div>
<script src="/js/canvasjs.min.js"></script>                           
<form action="" method="post">
  <div class="stat_recherche">
    <h1>Recherche :</h1>

  <select name="stat_refresh_etablissement" class="stat_recherche_etablissement" id="filtre_statut">
      <option selected value='tous'>----------</option>
            <?php
            $_query_result = mysqli_query($db_conn, "SELECT * FROM `etablissement`");
            while ($_query_row = mysqli_fetch_row($_query_result)) {
                if($_query_row[1] == $_POST["stat_refresh_etablissement"]) echo "<option selected value='$_query_row[1]'>$_query_row[1]</option>";
                else echo "<option value='$_query_row[1]'>$_query_row[1]</option>";
            }
            ?>
    </select>
    <select name="stat_refresh_service" class="stat_recherche_service" id="filtre_statut">
      <option selected value='tous'>----------</option>
            <?php
            $_query_result = mysqli_query($db_conn, "SELECT * FROM `service` WHERE etablissement LIKE '%| ".$_POST["stat_refresh_etablissement"]." |%'");
            while ($_query_row = mysqli_fetch_row($_query_result)) {
                if($_query_row[1] == $_POST["stat_refresh_service"]) echo "<option selected value='$_query_row[1]'>$_query_row[1]</option>";
                else echo "<option value='$_query_row[1]'>$_query_row[1]</option>";
            }
            ?>
    </select>
  <input name="stat_date" class="stat_recherche_date" type="number" value="<?php echo $default_date;?>">
  <button type="submit" class="stat_recherche_refresh">rafraîchir</button>
          </div>
</form>
  <?php } ?>

</body>

</HTML>

<script>
  <?php if ($div["admin_service"] == 1) { ?>
    var id;
    var modal;
    var span = document.getElementsByClassName("close")[0];

    function config_open(id) {

      span = document.getElementById(id);
      modal = document.getElementById(id);

      // Get the button that opens the modal
      var btn = document.getElementById(id);
      modal.style.display = "block";

    }

    function close_windows(id) {
      modal = document.getElementById(id);
      modal.style.display = "none";
    }
  <?php } ?>
  <?php if ($div["intervention"] == 1) { ?>

    function autocomplete(inp, arr) {
      /*the autocomplete function takes two arguments,🔧
      the text field element and an array of possible autocompleted values:*/
      var currentFocus;
      /*execute a function when someone writes in the text field:*/
      inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
          return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        if (val.length >= 3) {
          for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            a_ = arr[i].toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            b_ = val.toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            if ((a_.toUpperCase()).includes(b_)) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              /*make the matching letters bold:*/
              b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
              b.innerHTML += arr[i].substr(val.length);
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
              /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                //Récupération et setup du service :)
                var search = this.getElementsByTagName("input")[0].value;
                var result = service.filter(o => o.value === search);

                function getFields(input, field) {
                  var output = [];
                  for (var i = 0; i < input.length; ++i)
                    output.push(input[i][field]);
                  return output;
                }

                document.getElementById("service_list").value = getFields(result, "name")[0];
                if (document.getElementById('service_list').value !== getFields(result, "name")[0]) {
                  document.getElementById("service_list").value = "Votre service";

                }
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
              });
              a.appendChild(b);
            }
          }
        }

      });
      /*execute a function presses a key on the keyboard:*/
      inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
      });

      function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
      }

      function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
      }

      function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
          }
        }
      }
      /*execute a function when someone clicks in the document:*/
      document.addEventListener("click", function(e) {
        closeAllLists(e.target);
      });
    }
    /*An array containing all the country names in the world:*/
    var user = [<?php
    $count = 0;
    $query_result = mysqli_query(
        $db_conn,
        "SELECT * FROM `utilisateur` WHERE `etablissement` LIKE '%$etablissement_used%'"
    );
    while ($query_row = mysqli_fetch_row($query_result)) {
        $user_list[$count - 1] = $query_row[1];
        $service_list[$count - 1] = $query_row[2];

        $count++;
    }
      foreach ($user_list as $user) {
        echo '"' . $user . '", ';
    }
    ?>];

    var service = [<?php foreach ($user_list as $index => $value) {
        echo '{"value": "' .
            $user_list[$index] .
            '", "name": "' .
            $service_list[$index] .
            '"},';
    } ?>];

    /*initiate the autocomplete function on the "myInput" element, and pass along the user array as possible autocomplete values:*/
    autocomplete(document.getElementById("name_demand"), user);
    
    /*function mes_bon() {
      var name_demand = document.getElementById("name_demand").value;
      if (name_demand.length == 0) {
        alert("Merci de remplir votre nom prénom !");
      }else{
        document.location.href = "view/?method=post&name_demand=". name_demand;
      }
    }

    function mes_services() {
      var service_list = document.getElementById("service_list").value;
      if (service_list == "Votre service") {
        alert("Merci de selectionner votre service !");
      }else{
        document.location.href = "view/?method=post&service_list=". service_list;
      }
    }*/
  <?php } ?>
  <?php if ($div["intervention_view"] == 1) { ?>
    $(document).ready(function() {
      load_data("<?php echo $etablissement_used; ?>", "<?php echo $service_used; ?>", "25", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

      function load_data(etablissement, service, list_rows, filtre_nom, nom_filtre_nom, filtre_service, nom_filtre_service, filtre_date, nom_filtre_date, filtre_id, nom_filtre_id,filtre_type,nom_filtre_type,filtre_statut, nom_filtre_statut,filtre_tech,nom_filtre_tech,filtre_details,nom_filtre_details,filtre_remarque,nom_filtre_remarque) {
        $.ajax({
          url: "/fetch.php",
          method: "post",
          data: {
            service: service,
            etablissement: etablissement,
            list_rows: list_rows,
            filtre_nom: filtre_nom,
            nom_filtre_nom: nom_filtre_nom,
            filtre_service: filtre_service,
            nom_filtre_service: nom_filtre_service,
            filtre_date: filtre_date,
            nom_filtre_date: nom_filtre_date,
            filtre_id: filtre_id,
            nom_filtre_id: nom_filtre_id,
            filtre_type: filtre_type,
            nom_filtre_type: nom_filtre_type,
            filtre_statut: filtre_statut,
            nom_filtre_statut: nom_filtre_statut,
            filtre_tech: filtre_tech,
            nom_filtre_tech: nom_filtre_tech,
            filtre_details: filtre_details,
            nom_filtre_details: nom_filtre_details,
            filtre_remarque: filtre_remarque,
            nom_filtre_remarque: nom_filtre_remarque,

          },
          success: function(data) {
            $('#result').html(data);
          }
        });
      }
      const button = document.getElementById('reload_data_research');

      button.addEventListener('click', event => {
        if (!document.getElementById("intervention_view").hidden) {
          var filtre_nom_demandeur = document.getElementById("filtre_nom").value;
          var filtre_service = document.getElementById("filtre_service").value;
          var filtre_date = document.getElementById("filtre_date").value;
          var list_rows = document.getElementById("list_rows").value;
          var filtre_id = document.getElementById("filtre_id").value;
            var filtre_type = "";
            var filtre_statut = "";
            var filtre_statut = "";
            var filtre_tech = "";
            var filtre_details = "";
            var filtre_remarque = "";
          <?php if($_SESSION["authenticated"] == 1){ ?>
            var filtre_type = document.getElementById("filtre_type").value;
            var filtre_statut = document.getElementById("filtre_statut").value;
            if(filtre_statut == -1) filtre_statut = "";
            var filtre_tech = document.getElementById("filtre_tech").value;
            var filtre_details = document.getElementById("filtre_details").value;
            var filtre_remarque = document.getElementById("filtre_remarque").value;
          <?php }?>
          load_data("<?php echo $etablissement_used; ?>", "<?php echo $service_used; ?>", list_rows, filtre_nom_demandeur, "nom_demandeur", filtre_service, "service_demandeur", filtre_date, "date_demande", filtre_id, "id", filtre_type, "nature_demande", filtre_statut, "statut", filtre_tech, "nom_technicien", filtre_details, "reparation", filtre_remarque, "remarque");
        }
      });


    });

  <?php } ?>
  <?php if (
      $div["admin_service"] == 1 ||
      $div["intervention_ticket_view"] == 1
  ) { ?>
    $(document).on('click', ':not(form)[data-confirm]', function(e) {
      if (!confirm($(this).data('confirm'))) {
        e.stopImmediatePropagation();
        e.preventDefault();
      }
    });

    $(document).on('submit', 'form[data-confirm]', function(e) {
      if (!confirm($(this).data('confirm'))) {
        e.stopImmediatePropagation();
        e.preventDefault();
      }
    });

    $(document).on('input', 'select', function(e) {
      var msg = $(this).children('option:selected').data('confirm');
      if (msg != undefined && !confirm(msg)) {
        $(this)[0].selectedIndex = 0;
      }
    });
    $('select option').on('mousedown', function(e) {
      if (this.parentNode.getAttribute("id") == "etablissement_list_service") {
        this.selected = !this.selected;
        e.preventDefault();
      }
    });
  <?php } ?>
</script>
<?php mysqli_close($db_conn);
?>
