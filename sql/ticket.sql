-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 04 août 2021 à 18:45
-- Version du serveur : 10.5.11-MariaDB-1:10.5.11+maria~buster
-- Version de PHP : 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ticket`
--

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `id` int(150) NOT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `id` int(11) NOT NULL,
  `etablissement` varchar(25) DEFAULT NULL,
  `date_demande` varchar(25) DEFAULT NULL,
  `service` varchar(25) DEFAULT NULL,
  `service_demandeur` varchar(25) DEFAULT NULL,
  `nom_demandeur` varchar(50) DEFAULT NULL,
  `nom_technicien` varchar(50) DEFAULT NULL,
  `nature_demande` varchar(25) DEFAULT NULL,
  `reparation` longtext DEFAULT NULL,
  `statut` int(5) DEFAULT NULL,
  `date_prise_en_charge` varchar(25) DEFAULT NULL,
  `date_fin` varchar(25) DEFAULT NULL,
  `remarque` longtext DEFAULT NULL,
  `voir_personnel` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `nature_demande`
--

CREATE TABLE `nature_demande` (
  `id` int(150) NOT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `permission` varchar(150) DEFAULT NULL,
  `etablissement` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `etablissement` varchar(150) DEFAULT NULL,
  `mail` varchar(150) DEFAULT NULL,
  `Descriptions` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `service_demandeur`
--

CREATE TABLE `service_demandeur` (
  `id` int(11) NOT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `etablissement` varchar(150) DEFAULT NULL,
  `couleur` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `id` int(11) NOT NULL,
  `nom` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id`, `nom`) VALUES
(0, 'En attente'),
(1, 'En cours'),
(2, 'Terminer');

-- --------------------------------------------------------

--
-- Structure de la table `technicien`
--

CREATE TABLE `technicien` (
  `id` int(11) NOT NULL,
  `utilisateur` varchar(150) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL COMMENT '$2y$10$pTSuCdOPA/PN8vnWUlBOo./jmhd5u6hVB6RhkCHf6cHDFTrRcPjHq = test',
  `administrateur` smallint(15) DEFAULT NULL,
  `etablissement` varchar(150) DEFAULT NULL,
  `service` varchar(150) DEFAULT NULL,
  `mail` varchar(150) DEFAULT NULL,
  `password-to-change` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `technicien`
--

INSERT INTO `technicien` (`id`, `utilisateur`, `password`, `administrateur`, `etablissement`, `service`, `mail`, `password-to-change`) VALUES
(43, 'admin', '$2y$10$M1a/2lY2fuN5WlnT3KRsT.LC9v1hh8po0dUzXrxix1HQuWTugeCU.', 1, NULL, 'informatique', 'admin', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` text DEFAULT NULL,
  `service` text DEFAULT NULL,
  `etablissement` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `nature_demande`
--
ALTER TABLE `nature_demande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `service_demandeur`
--
ALTER TABLE `service_demandeur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `technicien`
--
ALTER TABLE `technicien`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `intervention`
--
ALTER TABLE `intervention`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `nature_demande`
--
ALTER TABLE `nature_demande`
  MODIFY `id` int(150) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `service_demandeur`
--
ALTER TABLE `service_demandeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `technicien`
--
ALTER TABLE `technicien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
